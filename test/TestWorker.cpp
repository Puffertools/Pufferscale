/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <iostream>
#include "pufferscale/Worker.hpp"
#include "pufferscale/BucketInfo.hpp"
#include "pufferscale/Callbacks.hpp"

namespace tl = thallium;
namespace ps = pufferscale;


bool migration(const ps::MigrationOperation& ops, void* uargs){
	(void) ops;
	(void) uargs;
    std::cout << "Migrate" << std::endl;
	return true;
}

int i = 1;
std::string addr;

ps::BucketMetadata update_meta(const ps::BucketTag& rt, void* uargs){
	(void) rt;
	(void) uargs;
    std::cout << "Migrate" << std::endl;
    std::cout << "Update metadata" << std::endl;
	ps::BucketMetadata rm;
	rm.m_bucket_data_in_memory = ++i;
	rm.m_bucket_load = 1;
	return rm;
}


bool terminate_provider(const ps::ProviderInfo& pi, void* uargs, void* cargs){
	(void) pi;
	(void) uargs;
	(void) cargs;
	
	return true;
}

ps::ProviderInitializationData initiate_provider(void* cargs){
	(void) cargs;
	ps::ProviderInitializationData pid;
	pid.m_provider_info.m_provider_id = 10;
	pid.m_provider_info.m_service_name = "DummyProvider";
	pid.m_provider_info.m_provider_address = addr;
	return pid;
}


int main(int argc, char** argv) {

    if(argc != 2) {
        std::cout << "Usage: " << argv[0] << " <MasterAddress>" << std::endl;
        exit(-1);
    }

    tl::engine myEngine("tcp", THALLIUM_SERVER_MODE);

	addr = myEngine.self();

    std::cout << "Worker running at address " << myEngine.self() << std::endl;
    
	// Setting up the worker
	ps::MasterInfo masterInfo;
    masterInfo.m_address = argv[1];

    ps::Worker worker(myEngine);

	// Setting properties
	worker.configure_worker(10,10);
	// Call rebalancing
	worker.register_provider("DummyProvider",
								nullptr,
								initiate_provider,
								terminate_provider);


	// Adding dummy buckets

    std::cout << "Preparing to add buckets" << std::endl;

	ps::ProviderInfo pi;	
	pi.m_provider_id = 64;
	pi.m_service_name = "DummyProvider";
	pi.m_provider_address = myEngine.self();
	
	ps::BucketTag rt;
	rt.m_service_name = pi.m_service_name;
	rt.m_bucket_id = 1;

	worker.manages_provider(pi);

	std::cout << "Adding buckets" << std::endl;

	worker.manages(pi, rt, &migration, &update_meta, nullptr);
    
	std::cout << "Buckets added" << std::endl;

	// Joining the master

    bool b = worker.join(masterInfo);
    if(b) {
        std::cout << "Worker joined successfully" << std::endl;
    } else {
        std::cout << "Worker failed to join" << std::endl;
    }

    myEngine.wait_for_finalize();

    return 0;
}
