/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "pufferscale/Master.hpp"

namespace tl = thallium;
namespace ps = pufferscale;

int main() {

    tl::engine myEngine("tcp", THALLIUM_SERVER_MODE);

    std::cout << "Master running at address " << myEngine.self() << std::endl;

    ps::Master master(myEngine);

    myEngine.wait_for_finalize();

    return 0;
}
