/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <thallium.hpp>
#include "pufferscale/Controller.hpp"
#include "pufferscale/BucketInfo.hpp"
#include "pufferscale/WorkerInfo.hpp"

namespace tl = thallium;
namespace ps = pufferscale;

int main(int argc, char** argv) {

    if(argc != 2) {
        std::cout << "Usage: " << argv[0] << " <MasterAddress>" << std::endl;
        exit(-1);
    }

    tl::engine myEngine("tcp", THALLIUM_CLIENT_MODE);

    ps::MasterInfo masterInfo;
    masterInfo.m_address = argv[1];

    ps::Controller controler(myEngine, masterInfo);
    
    auto buckets = controler.list_all_buckets();

	std::unordered_map<ps::WorkerInfo,std::vector<ps::BucketInfo>,ps::WorkerInfoHash>::iterator it;
	for (it = buckets.begin(); it != buckets.end(); ++it){
		std::cout << "Worker" << std::endl;
		for (const ps::BucketInfo& ri : it->second){
			std::cout << " - Bucket" << std::endl;
			std::cout << "     - bucket id: " << ri.m_bucket_tag.m_bucket_id << std::endl; 
			std::cout << "     - provider id: " << ri.m_provider_info.m_provider_id << std::endl; 
			std::cout << "     - provider name: " << ri.m_provider_info.m_service_name << std::endl; 
			std::cout << "     - provider address: " << ri.m_provider_info.m_provider_address << std::endl; 
		}
	}



	if (buckets.size() > 1){
		std::cout << "Decommissioning a node" << std::endl;
		ps::WorkerInfo wi = buckets.begin()->first;
		controler.decommission(wi);
	}

    buckets = controler.list_all_buckets();
	for (it = buckets.begin(); it != buckets.end(); ++it){
		std::cout << "Worker" << std::endl;
		for (const ps::BucketInfo& ri : it->second){
			std::cout << " - Bucket" << std::endl;
			std::cout << "     - bucket id: " << ri.m_bucket_tag.m_bucket_id << std::endl; 
			std::cout << "     - provider id: " << ri.m_provider_info.m_provider_id << std::endl; 
			std::cout << "     - provider name: " << ri.m_provider_info.m_service_name << std::endl; 
			std::cout << "     - provider address: " << ri.m_provider_info.m_provider_address << std::endl; 
		}
	}

    return 0;
}
