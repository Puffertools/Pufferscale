/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <thallium.hpp>

#include "pufferscale/Master.hpp"
#include "pufferscale/Worker.hpp"
#include "pufferscale/Controller.hpp"
#include "pufferscale/BucketInfo.hpp"
#include "pufferscale/Callbacks.hpp"

#include <string>
#include <memory>

namespace tl = thallium;
namespace ps = pufferscale;

int nb_workers = 512;
int nb_buckets_per_provider = 64;
int nb_workers_to_decom = 256;

std::vector<ps::ProviderInfo> providers(nb_workers);
std::vector<ps::BucketTag> buckets(nb_workers*nb_buckets_per_provider);

std::vector<double> load(nb_workers);
std::vector<double> data(nb_workers);
std::vector<double> recv(nb_workers);
std::vector<double> send(nb_workers);

std::vector<std::shared_ptr<ps::Worker>> workers;

std::unordered_map<ps::BucketTag, double, ps::BucketTagHash> load_per_bucket;
std::unordered_map<ps::BucketTag, double, ps::BucketTagHash> size_per_bucket;

ps::BucketMetadata update_meta(const ps::BucketTag& rt, void* uargs){
	(void) uargs;
	ps::BucketMetadata rm;
	rm.m_bucket_data_in_memory = size_per_bucket[rt];
	rm.m_bucket_load = load_per_bucket[rt];
	return rm;
}

bool migration(const ps::MigrationOperation& ops, void* uargs){
	(void) uargs;
	/*
	std::cout << "Migrate " << ops.m_bucket << "(";
	std::cout << load_per_bucket[ops.m_bucket] << ",";
	std::cout << size_per_bucket[ops.m_bucket];
	std::cout << ") from " << ops.m_source.m_provider_id;
	std::cout << " to " << ops.m_destination.m_provider_id << std::endl;
	//*/

	load[ops.m_source.m_provider_id] 		-= load_per_bucket[ops.m_bucket];
	send[ops.m_source.m_provider_id] 		+= size_per_bucket[ops.m_bucket];
	data[ops.m_source.m_provider_id] 		-= size_per_bucket[ops.m_bucket];
	load[ops.m_destination.m_provider_id] 	+= load_per_bucket[ops.m_bucket];
	recv[ops.m_destination.m_provider_id] 	+= size_per_bucket[ops.m_bucket];
	data[ops.m_destination.m_provider_id] 	+= size_per_bucket[ops.m_bucket];

	workers[ops.m_destination.m_provider_id]->manages(ops.m_destination,ops.m_bucket,&migration,&update_meta,nullptr);

	return true;
}

std::string addr;

bool terminate_provider(const ps::ProviderInfo& pi, void* uargs, void* cargs){
	(void) pi;
	(void) uargs;
	(void) cargs;
	
	return true;
}

ps::ProviderInitializationData initiate_provider(void* cargs){
	ps::ProviderInitializationData pid;
	pid.m_provider_info.m_provider_id = *(int*)(cargs);
	pid.m_provider_info.m_service_name = "DummyProvider";
	pid.m_provider_info.m_provider_address = addr;
	return pid;
}



int main(){
	std::cout << "Testing decommission" << std::endl;
	tl::engine myEngine("tcp", THALLIUM_SERVER_MODE);

	addr = myEngine.self();
	
	// Starting the master
	ps::Master master(myEngine);

	// MasterInfo
	ps::MasterInfo masterInfo;
    masterInfo.m_address = myEngine.self();
	
	std::vector<int> id_nb;

	for (int i = 0; i < nb_workers; ++i)
		id_nb.push_back(i);

	for (int i = 0; i < nb_workers; ++i){
		workers.push_back(std::shared_ptr<ps::Worker>(new ps::Worker(myEngine,i)));
		workers[i]->configure_worker(10000000,1000);
		
		
		workers[i]->register_provider("DummyProvider",
										(void*)(id_nb.data()+i),
										initiate_provider,
										terminate_provider);

		providers[i].m_provider_id = i;
		providers[i].m_service_name = "DummyProvider";
		providers[i].m_provider_address = myEngine.self();
	
		workers[i]->manages_provider(providers[i]);
	
		for (int j = 0; j < nb_buckets_per_provider; ++j){
			ps::BucketTag& rt = buckets[i*nb_buckets_per_provider+j];
			
			rt.m_bucket_id = i*nb_buckets_per_provider+j;
			rt.m_service_name = providers[i].m_service_name;
				
			size_per_bucket[rt] = j+1;
			load_per_bucket[rt] = j+1; //nb_buckets_per_provider-j;
				
			workers[i]->manages(providers[i],buckets[i*nb_buckets_per_provider+j], &migration, &update_meta,nullptr);
			load[i] += load_per_bucket[rt];
			data[i] += size_per_bucket[rt];
		}

    	bool b = workers[i]->join(masterInfo);
    	if(b) {
//    		std::cout << "Worker " << i << " joined successfully [" << load[i] << ", " <<  data[i] << "]" << std::endl;
    	} else {
        	std::cout << "Worker " << i << " failed to join" << std::endl;
    	}
			
	}

	// Controller
	
    ps::Controller controller(myEngine, masterInfo);

	controller.list_all_buckets();

	std::vector<ps::WorkerInfo> to_decom(nb_workers_to_decom);
	for (int i = 0; i < nb_workers_to_decom; ++i){
		to_decom[i].m_address = myEngine.self();
		to_decom[i].m_provider_id = i;
	}

	controller.decommission(to_decom);

//	std::cout << "ID\tLoad\tData\tRecv\tsend" << std::endl;
	for (int i = 0; i < nb_workers; ++i){
		//std::cout << i << "\t" << load[i] << "\t" << data[i] << "\t" << recv[i] << "\t" << send[i] << std::endl;
	}

	myEngine.finalize();

	return 0;
}




