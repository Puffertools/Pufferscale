/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_CONTROLLER_H
#define __PUFFERSCALE_CONTROLLER_H

#include <string>
#include <memory>
#include <thallium.hpp>
#include <thallium/serialization/stl/vector.hpp>
#include <thallium/serialization/stl/pair.hpp>
#include <thallium/serialization/stl/unordered_map.hpp>
#include <pufferscale/Identifiers.hpp>
#include <pufferscale/WorkerInfo.hpp>
#include <pufferscale/MasterInfo.hpp>
#include <pufferscale/ProviderInfo.hpp>
#include <pufferscale/ProviderMetadata.hpp>
#include <pufferscale/BucketInfo.hpp>
#include <pufferscale/ReturnCode.hpp>

namespace tl = thallium;

namespace pufferscale {

class Controller {

    private:

	tl::engine& m_engine;

	/// Handle to the master node
    std::unique_ptr<tl::provider_handle> m_master;
    
	/// RPC to master to commission providers on workers
	tl::remote_procedure m_commission;
	/// RPC to master to decommission workers
    tl::remote_procedure m_decommission;
	/// RPC to master to decommission specific providers
    tl::remote_procedure m_decommission_provider;
	/// RPC to master to list all buckets managed by Pufferscale
    tl::remote_procedure m_list_all_buckets;
	/// RPC to master to list all providers managed by Pufferscale
    tl::remote_procedure m_list_all_providers;

	/**
	 * @brief Interpret the content of a return code.
	 * Raise the matching exception if any error is returned.
	 *
	 * @param rc ReturnCode of a RPC
	 * @return true without error, raise the matching exception
	 * if other cases.
	 */
	bool interpret_return_code(const ReturnCode& rc) const;

    public:

    /**
     * @brief Initializes a Controller pointing to a particular Master instance.
     *
     * @param engine thallium engine (initialized as either a client or 
	 * a server).
     * @param master MasterInfo of the Master instance.
     */
    Controller(tl::engine& engine,  const MasterInfo& master);

    /**
     * @brief Destructor.
     */
    ~Controller();

    /**
     * @brief Method to call to inform the Master that a new Worker 
     * (with provided information in the workerInfo argument) is available
     * for commission.
     *
     * @param workerInfo Information on the Worker to commission.
     * @param providerInfo Name of the providers that should be started on
	 * the nodes.
     *
     * @return true if the commission is successful, false otherwise.
     */
    bool commission(
            const WorkerInfo& workerInfo,
            const std::vector<std::string>& providerInfo) const;

    /**
     * @brief Method to call to inform the Master that a set of new Workers
     * (with provided information in the workers argument) is available
     * for commission.
     *
     * @param workers Mapping of worker information to the list of 
	 * providers names to commission.
     *
     * @return true if the commission is successful, false otherwise.
     */
    bool commission(
            const std::unordered_map<WorkerInfo, 
					std::vector<std::string>, 
					WorkerInfoHash>& workers) const;

    /**
     * @brief Method to call to inform the Master that a set of providers 
	 * on a Worker should be decommissioned.
     *
     * @param workerInfo Information on the Worker.
     * @param providerInfo Information on the providers to decommission.
     *
     * @return true if the decommission is successful, false otherwise.
     */
    bool decommission(
            const WorkerInfo& workerInfo,
            const std::vector<ProviderInfo>& providerInfo) const;

    /**
     * @brief Method to call to inform the Master that a set of providers
	 * on specified workers should be decommissioned.
     *
     * @param workers Mapping of worker information to the list of providers 
	 * to decommission.
	 *
     * @return true if the decommission is successful, false otherwise.
     */
    bool decommission(
            const std::unordered_map<WorkerInfo, 
					std::vector<ProviderInfo>,
					WorkerInfoHash>& workers) const;

    /**
     * @brief Method to call to request the Master to decommission a given Worker
     * (with provided information in the workerInfo argument).
     *
     * @param workerInfo Information on the Worker to decommission.
     *
     * @return true if the decommission is successful, false otherwise.
     */
    bool decommission(
            const WorkerInfo& workerInfo) const;

    /**
     * @brief Method to call to request the Master to decommission a given set of
	 * Workers
     * (with provided information in the workers argument).
     *
     * @param workers Information on the Workers to decommission.
     *
     * @return true if the decommission is successful, false otherwise.
     */
    bool decommission(
            const std::vector<WorkerInfo>& workers) const;

    /**
     * @brief Requests the Master to send the current list of all buckets.
     *
     * @return the map of all Workers and associated buckets known to the 
	 * Master.
     */
    std::unordered_map<WorkerInfo,
		std::vector<BucketInfo>,
		WorkerInfoHash> list_all_buckets() const;

    /**
     * @brief Requests the Master to send the current list of all providers.
     *
     * @return the map of all Workers and associated providers known to the 
	 * Master.
     */
	std::unordered_map<WorkerInfo,
		std::vector<ProviderInfo>,
		WorkerInfoHash> list_all_providers() const;
};

}

#endif
