/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_MIGRATION_PLAN_H
#define __PUFFERSCALE_MIGRATION_PLAN_H

#include <vector>
#include <thallium/serialization/stl/vector.hpp>
#include <pufferscale/BucketTag.hpp>
#include <pufferscale/ProviderInfo.hpp>

namespace pufferscale {

/**
 * Structures that indicates where to send buckets during a rescaling
 * operation.
 */

/**
 * Structure that describes the transfer of a single bucket.
 */
struct MigrationOperation {
   
	/// Identifier of the bucket to move
    BucketTag  m_bucket;
	/// Provider that hosts it initially
    ProviderInfo m_source;
	/// Provider that must receive it
	ProviderInfo m_destination;

    template<typename A>
    void serialize(A& ar) {
        ar & m_bucket;
        ar & m_source;
		ar & m_destination;
    }
};

/**
 * Set of transfer descriptions.
 */
struct MigrationPlan {

	/// Vector of transfers, one per bucket to move
    std::vector<MigrationOperation> m_operations;

    template<typename A>
    void serialize(A& ar) {
        ar & m_operations;
    }
};

}

#endif
