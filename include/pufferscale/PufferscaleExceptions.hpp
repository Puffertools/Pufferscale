/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_PUFFERSCALE_EXCEPTIONS_H
#define __PUFFERSCALE_PUFFERSCALE_EXCEPTIONS_H

#include <exception>
#include <iostream>
#include <sstream>
#include <string>

#include <pufferscale/ProviderInfo.hpp>
#include <pufferscale/BucketTag.hpp>
#include <pufferscale/BucketTag.hpp>
#include <pufferscale/WorkerInfo.hpp>

namespace pufferscale{


class TooMuchDataException: public std::exception {
private:
	std::string service_name;

public:
	TooMuchDataException(std::string pn):
		service_name(pn){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "There are too much data on provider " << service_name;
		oss << " compared to the capacity of the workers.";
		return oss.str().c_str();
	}
};

class NoDiskException: public std::exception {
private:
	WorkerInfo worker;
	std::string service_name;

public:
	NoDiskException(WorkerInfo wi, std::string pn):
		worker(wi),
		service_name(pn){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Provider " << service_name << " requiers a disk and thus"; 
		oss << " cannot be commissioned on " << worker;
		return oss.str().c_str();
	}
};

class AlreadyKnownWorkerException: public std::exception {
private:
	WorkerInfo worker;

public:
	AlreadyKnownWorkerException(WorkerInfo wi):
		worker(wi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Worker " << worker << " has already joined.";
		return oss.str().c_str();
	}
};

class UnknownWorkerException: public std::exception {
private:
	WorkerInfo worker;

public:
	UnknownWorkerException(WorkerInfo wi):
		worker(wi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Worker " << worker << " has not joined yet.";
		return oss.str().c_str();
	}
};

class UnknownBucketException: public std::exception {
private:
	WorkerInfo worker;
	BucketTag bucket;

public:
	UnknownBucketException(WorkerInfo wi, BucketTag ri):
		worker(wi),
		bucket(ri){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Bucket " << bucket << " is unknown by " <<worker << ".";
		return oss.str().c_str();
	}
};

class IncorrectNetworkBandwidthException: public std::exception {
private:
	double bw;
	WorkerInfo worker;

public:
	IncorrectNetworkBandwidthException(WorkerInfo wi, double v):
		bw(v),
		worker(wi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The network bandwith of worker " << worker;
		oss << " is set to " << bw << " but cannot be negative or null.";
		return oss.str().c_str();
	}
};

class IncorrectReadBandwidthException: public std::exception {
private:
	double bw;
	WorkerInfo worker;

public:
	IncorrectReadBandwidthException(WorkerInfo wi, double v):
		bw(v),
		worker(wi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The read bandwith of the disk of worker " << worker;
		oss <<  " is set to " << bw << " but cannot be negative or null.";
		return oss.str().c_str();
	}
};
class IncorrectWriteBandwidthException: public std::exception {
private:
	double bw;
	WorkerInfo worker;

public:
	IncorrectWriteBandwidthException(WorkerInfo wi, double v):
		bw(v),
		worker(wi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The write bandwith of the disk of worker " << worker;
		oss <<  " is set to " << bw << " but cannot be negative or null.";
		return oss.str().c_str();
	}
};

class IncorrectDiskCapacityException: public std::exception {
private:
	double c;
	WorkerInfo worker;

public:
	IncorrectDiskCapacityException(WorkerInfo wi, double v):
		c(v),
		worker(wi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The disk capacity of " << worker <<  " is set to " << c << " but cannot be negative or null.";
		return oss.str().c_str();
	}
};

class IncorrectMemoryCapacityException: public std::exception {
private:
	double c;
	WorkerInfo worker;

public:
	IncorrectMemoryCapacityException(WorkerInfo wi, double v):
		c(v),
		worker(wi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The memory capacity of " << worker <<  " is set to " << c << " but cannot be negative or null.";
		return oss.str().c_str();
	}
};

class WorkerManagingBucketsException: public std::exception {
private:
	WorkerInfo worker;

public:
	WorkerManagingBucketsException(WorkerInfo wi):
		worker(wi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Worker "<< worker << " is still managing buckets and cannot be shutdown.";
		return oss.str().c_str();
	}
};

class ProviderNotPresentException: public std::exception {
private:
	WorkerInfo worker;
	ProviderInfo provider;

public:
	ProviderNotPresentException(WorkerInfo wi, ProviderInfo pi):
		worker(wi),
		provider(pi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Provider " << provider << " is not on worker ";
		oss <<  worker << ".";
		return oss.str().c_str();
	}
};

class ProviderAlreadyPresentException: public std::exception {
private:
	WorkerInfo worker;
	ProviderInfo provider;

public:
	ProviderAlreadyPresentException(WorkerInfo wi, ProviderInfo pi):
		worker(wi),
		provider(pi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Provider " << provider << " is already runing on worker ";
		oss <<  worker << " and cannot be commissioned.";
		return oss.str().c_str();
	}
};

class BucketAlreadyPresentException: public std::exception {
private:
	WorkerInfo worker;
	BucketTag bucket;

public:
	BucketAlreadyPresentException(WorkerInfo wi, BucketTag ri):
		worker(wi),
		bucket(ri){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Bucket " << bucket << " is already managed by worker ";
		oss <<  worker << " and cannot be added again.";
		return oss.str().c_str();
	}
};

class BucketNotPresentException: public std::exception {
private:
	WorkerInfo worker;
	BucketTag bucket;

public:
	BucketNotPresentException(WorkerInfo wi, BucketTag ri):
		worker(wi),
		bucket(ri){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Bucket " << bucket << " is not managed by worker ";
		oss <<  worker << " and cannot be removed.";
		return oss.str().c_str();
	}
};

class ProviderConstraintsException: public std::exception {
private:
	WorkerInfo worker;
	ProviderInfo provider;

public:
	ProviderConstraintsException(WorkerInfo wi, ProviderInfo pi):
		worker(wi),
		provider(pi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The constraints of provider " << provider << " does not allow it to be commissioned ";
		oss << " on worker " << worker << ".";
		return oss.str().c_str();
	}
};

class NoProviderToMoveBucketException: public std::exception {
private:
	BucketTag bucket;
	WorkerInfo worker;

public:
	NoProviderToMoveBucketException(BucketTag ri, WorkerInfo wi):
		bucket(ri),
		worker(wi){}
	
	const char* what() const noexcept{
		std::ostringstream oss;
		oss << "No provider is available to move bucket " << bucket;
		oss << " out of worker " << worker << ".";
		std::string msg = oss.str();
		return msg.c_str();
	}
};

class BucketNotMigratedException: public std::exception {
private:
	BucketTag bucket;
	WorkerInfo worker;

public:
	BucketNotMigratedException(WorkerInfo wi, BucketTag ri):
		bucket(ri),
		worker(wi){}
	
	const char* what() const noexcept{
		std::ostringstream oss;
		oss << "Could not migrate bucket " << bucket;
		oss << " out of worker " << worker << ".";
		std::string msg = oss.str();
		return msg.c_str();
	}
};

class ProviderCannotBeDecommissionedException: public std::exception {
private:
	WorkerInfo worker;
	ProviderInfo provider;

public:
	ProviderCannotBeDecommissionedException(WorkerInfo wi, ProviderInfo pi):
		worker(wi),
		provider(pi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Provider " << provider << " cannot be decommissioned from ";
		oss << "worker " << worker << " because it hosts buckets.";
		return oss.str().c_str();
	}
};

class ProviderTypeAlreadyKnownException: public std::exception {
private:
	WorkerInfo worker;
	std::string provider;

public:
	ProviderTypeAlreadyKnownException(WorkerInfo wi, std::string pi):
		worker(wi),
		provider(pi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The callbacks for provider type " << provider << " have already been set ";
		oss << "for worker " << worker << ".";
		return oss.str().c_str();
	}
};

class ProviderTypeUnknownException: public std::exception {
private:
	WorkerInfo worker;
	std::string provider;

public:
	ProviderTypeUnknownException(WorkerInfo wi, std::string pi):
		worker(wi),
		provider(pi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The callbacks for provider type " << provider << " have not been set ";
		oss << "for worker " << worker << ".";
		return oss.str().c_str();
	}
};

class ProviderNotCommissionedException: public std::exception {
private:
	WorkerInfo worker;
	std::string provider;

public:
	ProviderNotCommissionedException(WorkerInfo wi, std::string pi):
		worker(wi),
		provider(pi){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "Provider of " << provider << " have not been commissioned ";
		oss << "on worker " << worker << ".";
		return oss.str().c_str();
	}
};

class CallbackNotCallableException: public std::exception {
public:
	CallbackNotCallableException(){}
	
	virtual const char* what() const throw(){
		std::ostringstream oss;
		oss << "The provided callback is not callable";
		return oss.str().c_str();
	}
};

}

#endif
