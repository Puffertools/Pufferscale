/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_WORKER_QUOTAS_H
#define __PUFFERSCALE_WORKER_QUOTAS_H

#include <iostream>

#include <thallium.hpp>
#include <thallium/serialization/stl/string.hpp>

#include <pufferscale/Identifiers.hpp>

/**
 * Structure to store limits set on network, disk capacity, and
 * ram capacity for single service on a worker.
 */

namespace pufferscale {

struct WorkerQuotas {

	/// Expected available network bandwidth
	double m_bandwidth_network = 0;
	/// Expected available disk read bandwidth
	double m_bandwidth_disk_read = 0;
	/// Expected available disk write bandiwth
	double m_bandwidth_disk_write = 0;
	/// Maximum amount of data that can be stored on disk
	double m_quota_disk = 0;
	/// Maximum amount of data that can be stored in memory
	double m_quota_memory = 0;


    template<typename A>
    void serialize(A& ar) {
		ar & m_bandwidth_network;
		ar & m_bandwidth_disk_read;
		ar & m_bandwidth_disk_write;
		ar & m_quota_disk;
		ar & m_quota_memory;
 	}

	friend std::ostream& operator<<(std::ostream& os, const WorkerQuotas& wi){
		os << "WorkerQuotas[";
		os << "[net_bw:" << wi.m_bandwidth_network;
		os << ", rd_bw:" << wi.m_bandwidth_disk_read << ", rd_bw:" << wi.m_bandwidth_disk_write;
		os << ", cap_disk:" << wi.m_quota_disk;
		os << ", cap_mem:" << wi.m_quota_memory << "]";
		return os;
	}
};

}

#endif
