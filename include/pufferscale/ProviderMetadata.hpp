/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_PROVIDER_METADATA_H
#define __PUFFERSCALE_PROVIDER_METADATA_H

#include <thallium.hpp>
#include <pufferscale/ProviderInfo.hpp>

/**
 * Dynamic info about a provider
 */
namespace pufferscale {

struct ProviderMetadata {

	/// Identifier of the provider
	ProviderInfo 	m_provider_info;
	/// Space on disk taken by the buckets managed by the provider
	double 			m_data_on_disk;
	/// Space in memory taken by the buckets managed by the provider
	double 			m_data_in_memory;
	/// Sum of the load of the buckets managed by the provider
	double 			m_total_load;
	/// Largest load out of the load of the buckets managed by the provider
	double 			m_max_load;
	/// Largest space taken on disk by any one of the buckets managed by the provider
	double			m_max_data_disk;
	/// Largest space taken in memory by any one of the buckets managed by the provider
	double 			m_max_data_mem;


    template<typename A>
    void serialize(A& ar) {
		ar & m_provider_info;
    	ar & m_data_on_disk;
    	ar & m_data_in_memory;
		ar & m_total_load;
		ar & m_max_load;
		ar & m_max_data_disk;
		ar & m_max_data_mem;
	}
	
	friend std::ostream& operator<<(std::ostream& os, const ProviderMetadata& pm){
		os << "ProviderMetadata[" << pm.m_provider_info << "," << pm.m_data_on_disk;
		os << "," << pm.m_data_in_memory << "," << pm.m_total_load <<  "]";
		return os;
	}	
};

}

#endif
