/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_WORKER_METADATA_H
#define __PUFFERSCALE_WORKER_METADATA_H

#include <iostream>

#include <thallium.hpp>
#include <thallium/serialization/stl/string.hpp>

#include <pufferscale/Identifiers.hpp>

namespace pufferscale {

struct WorkerMetadata {

	bool m_has_disk = false;
	double m_bandwidth_network = 0;
	double m_bandwidth_disk_read = 0;
	double m_bandwidth_disk_write = 0;
	double m_capacity_disk = 0;
	double m_capacity_memory = 0;


    template<typename A>
    void serialize(A& ar) {
		ar & m_bandwidth_network;
		ar & m_bandwidth_disk_read;
		ar & m_bandwidth_disk_write;
		ar & m_capacity_disk;
		ar & m_capacity_memory;
 		ar & m_has_disk;
 	}

	friend std::ostream& operator<<(std::ostream& os, const WorkerMetadata& wi){
		os << "WorkerMetadata[";
		os << "[disk:" << wi.m_has_disk << ", net_bw:" << wi.m_bandwidth_network;
		os << ", rd_bw:" << wi.m_bandwidth_disk_read << ", rd_bw:" << wi.m_bandwidth_disk_write;
		os << ", cap_disk:" << wi.m_capacity_disk;
		os << ", cap_mem:" << wi.m_capacity_memory << "]";
		return os;
	}
};

}

#endif
