/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_BUCKET_METADATA_H
#define __PUFFERSCALE_BUCKET_METADATA_H

#include <thallium.hpp>

/**
 * Dynamic info about a bucket
 */
namespace pufferscale {

struct BucketMetadata {

	/// Space taken on disk to store the bucket
	double 			m_bucket_data_on_disk; 
	/// Space taken in memory by the bucket
	double			m_bucket_data_in_memory;

	/**
	 * Load induced by the bucket on the provider hosting it.
	 * A low load indicates that managing the bucket has little impact
	 * on the provider.
	 *
	 * Must be positive, non null.
	 * The algorithms for commission and decommission will try to 
	 * minimize the sum of m_bucket_load on each provider
	 */
	double 			m_bucket_load;

    template<typename A>
    void serialize(A& ar) {
    	ar & m_bucket_data_on_disk;
    	ar & m_bucket_data_in_memory;
		ar & m_bucket_load;
	}
	
	friend std::ostream& operator<<(std::ostream& os, const BucketMetadata& rm){
		os << "BucketMetadata[" << rm.m_bucket_data_on_disk << ",";
		os << rm.m_bucket_data_in_memory << "," << rm.m_bucket_load <<  "]";
		return os;
	}	
};

}

#endif

