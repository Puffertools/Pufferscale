/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __INTERNALS_H
#define __INTERNALS_H

#include <string>
#include <vector>

#include <thallium.hpp>
#include <thallium/serialization/stl/vector.hpp>

#include <pufferscale/ProviderInfo.hpp>
#include <pufferscale/ProviderMetadata.hpp>
#include <pufferscale/BucketInfo.hpp>
#include <pufferscale/WorkerInfo.hpp>

namespace pufferscale {

struct ProviderSnapshot {

    WorkerInfo 					m_worker_info;
	std::vector<BucketInfo> 	m_buckets;
	ProviderMetadata			m_provider_metadata;
	ProviderInfo				m_provider_info;

    template<typename A>
    void serialize(A& ar) {
        ar & m_worker_info;
        ar & m_buckets;
        ar & m_provider_info;
        ar & m_provider_metadata;
	}

};	

}
#endif

