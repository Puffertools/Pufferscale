/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_WORKER_INFO_H
#define __PUFFERSCALE_WORKER_INFO_H

#include <iostream>
#include <string>
#include <functional>

#include <thallium.hpp>
#include <thallium/serialization/stl/string.hpp>

#include <pufferscale/Identifiers.hpp>

namespace pufferscale {

/**
 * Structure used to store information about the workers.
 */

struct WorkerInfo {

	/// Address of the worker
    std::string m_address;
	/// Provider_id used by margo to establish the connection  
    ProviderID  m_provider_id = 0;

    template<typename A>
    void serialize(A& ar) {
        ar & m_address;
        ar & m_provider_id;
	}

    bool operator==(const WorkerInfo& other) const {
        return m_provider_id == other.m_provider_id
            && m_address == other.m_address;
    }

	friend std::ostream& operator<<(std::ostream& os, const WorkerInfo& wi){
		os << "WorkerInfo[" << wi.m_address << ", " << wi.m_provider_id << "]";
		return os;
	}
};

struct WorkerInfoHash {

    typedef std::size_t result_type;

    std::hash<std::string> m_h3;
    std::hash<ProviderID>  m_h4;

    result_type operator()(const WorkerInfo& w) const {
        return m_h3(w.m_address) ^ m_h4(w.m_provider_id);
    }
};

}

#endif
