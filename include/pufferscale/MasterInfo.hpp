/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_MASTER_INFO_H
#define __PUFFERSCALE_MASTER_INFO_H

#include <thallium.hpp>
#include <thallium/serialization/stl/string.hpp>

#include <pufferscale/Identifiers.hpp>

namespace pufferscale {

/**
 * Informations about the master of Pufferscale
 */

struct MasterInfo {

	/// Address of the master
    std::string m_address;
	/// Provider_id used by margo to establish connection
    ProviderID  m_provider_id = 1;

    template<typename A>
    void serialize(A& ar) {
        ar & m_address;
        ar & m_provider_id;
    }

    bool operator==(const MasterInfo& other) const {
        return m_provider_id == other.m_provider_id
            && m_address == other.m_address;
    }
};

}

#endif
