/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_MASTER_H
#define __PUFFERSCALE_MASTER_H

#include <unordered_map>
#include <memory>
#include <chrono>

#include <thallium.hpp>
#include <thallium/serialization/stl/vector.hpp>
#include <thallium/serialization/stl/unordered_map.hpp>

#include <pufferscale/Internals.hpp>
#include <pufferscale/MigrationPlan.hpp>
#include <pufferscale/ProviderInfo.hpp>
#include <pufferscale/ProviderMetadata.hpp>
#include <pufferscale/BucketInfo.hpp>
#include <pufferscale/BucketMetadata.hpp>
#include <pufferscale/ReturnCode.hpp>
#include <pufferscale/WorkerInfo.hpp>
#include <pufferscale/WorkerMetadata.hpp>
#include <pufferscale/WorkerQuotas.hpp>

namespace tl = thallium;

namespace pufferscale {

class Master : public tl::provider<Master> {

    private:

	/// RPC to Worker to execute a migration plan
    tl::remote_procedure m_migrate;
	/// RPC to Worker to list all buckets managed by the node
    tl::remote_procedure m_list_all_buckets;
	/// RPC to Worker to list all buckets of a specific provider
    tl::remote_procedure m_list_provider_buckets;
	/// RPC to Worker to list all providers on the node
    tl::remote_procedure m_list_providers;
	/// RPC to Worker to initiate a rescaling
    tl::remote_procedure m_initiate_rescaling;
	/// RPC to Worker to terminate a rescaling
    tl::remote_procedure m_terminate_rescaling;

	/// Mutex that prevent some operations to happen in parallel
	std::unique_ptr<tl::mutex> m_op_mtx;

	/// Struct containing the scheduling parameters for a 
	/// provider type.
	struct ProviderRescalingParameters {
		/// Weight of the load-balancing
		double weight_load = 1;
		/// Weight of the data-balancing
		double weight_data = 1;
		/// Weight on the speed of the transfers
		double weight_transfers = 1;

		/// Whether the disk is used by the provider
		bool use_disk = false;
		/// Whether the memory is used to store data
		bool use_mem = true;

		/// Number of parallel data transfers sent from one
		/// Worker
		int parallel_migrations = 10;
	};

	/// Map of provider names and parameters
	std::unordered_map<std::string,ProviderRescalingParameters> 
		provider_parameters;

	/**
	 * Structure containing all information about a Worker
	 */ 
	struct WorkerData {
		/// Handle 
		std::shared_ptr<tl::provider_handle> 	m_handle;
		/// Worker metadata
		WorkerMetadata 							m_worker_metadata;
	};

	/**
	 * Map of all informations about the workers. 
	 */
	std::unordered_map<WorkerInfo, WorkerData, WorkerInfoHash> m_workers;
	/// Mutex to access m_workers
	std::unique_ptr<tl::mutex> m_workers_mtx;
	
	/// Counter for the requests of metadata updates
	int m_next_token = 0;
	/// Mutex to access m_next_token
	std::unique_ptr<tl::mutex> m_next_token_mtx;

	/// On_list_all_buckets buffers
	std::unordered_map<WorkerInfo,
		std::vector<BucketInfo>,WorkerInfoHash> m_all_buckets;
	/// On_list_all_buckets last token
	int m_all_buckets_token = 0;
	/// Mutex to access all_buckets
	std::unique_ptr<tl::mutex> m_all_buckets_mtx;


	/// On_list_providers buffer
	std::unordered_map<WorkerInfo,
			std::vector<ProviderInfo>,
			WorkerInfoHash> m_all_providers;
	/// On_list_providers last token
	int m_all_providers_token = 0;
	/// Mutex to access all_providers
	std::unique_ptr<tl::mutex> m_all_providers_mtx;

	/// Duration of last scheduling
	double m_duration_last_scheduling = 0;

	/**	
	 * @brief List all buckets that are managed by the workers
	 *
	 * @return Mapping of Workers to the buckets they host
	 */
	std::unordered_map<WorkerInfo,std::vector<BucketInfo>,WorkerInfoHash> 
		get_all_buckets();

	/**
	 * @brief Get all buckets on the providers passed in
	 * argument.
	 *
	 * @param providers Mapping of Workers to providers that needs to
	 * have their buckets listed.
	 *
	 * @return Mapping of list of buckets to provider 
	 */
	std::unordered_map<ProviderInfo,
			ProviderSnapshot,ProviderInfoHash> get_all_provider_buckets(
										const std::string& service_name,
										double weight_load,
										double weight_data,
										const std::vector<WorkerInfo>& hosts);

	/**
	 * @brief Returns the list of providers and their metadata
	 *
	 * @return Mapping of Workers to a list of providers and their metadata
	 */
	std::unordered_map<WorkerInfo,
			std::unordered_map<ProviderInfo,
					ProviderMetadata,
					ProviderInfoHash>,
			WorkerInfoHash> get_all_providers();

	
	/**
	 * @brief Called upon reception of "pufferscale_list_all_providers_master"
	 * List the providers on Workers
	 *
	 * @param req Thallium request
	 * @param buff_size Size of the buffer to send back information
	 * @param remote_bulk Bulk to send back information
	 */
	void on_list_providers(
			const tl::request& req, 
			size_t buff_size, 
			tl::bulk remote_bulk,
			int request_id);


    /**
     * @brief This method is called when receiving a "pufferscale_join"
     * RPC from a Worker instance. It provides the the metadata about
	 * the worker.
     *
     * @param workerInfo Information on the worker that sent the RPC.
	 * @param workerMetadata Metadata about the worker that sent the RPC.
	 *
	 * @return A ReturnCode indicating the success of the join operation.
     */
    ReturnCode on_join(
            const WorkerInfo& workerInfo,
            const WorkerMetadata& workerMetadata);

    /**
     * @brief This method is called when receiving a "pufferscale_leave"
     * RPC from a Worker instance. The Worker requests to leave the 
	 * cluster: it is granted if there are no rescaling operations running.
     *
     * @param workerInfo Information on the worker that sent the RPC.
	 *
	 * @return A ReturnCode indicating the success of the leave operation.
     */
	ReturnCode on_leave(const WorkerInfo& workerInfo);

	

    /**
     * @brief This method is called when receiving a "pufferscale_commission"
     * RPC from a Controller instance. The Controller instances request the 
	 * initialization of providers on the specified nodes.
     * The workers must have joined the Master beforehand. 
     *
     * @param workers table mapping WorkerInfo to the provider names to start in these Workers.
     *
	 * @return A ReturnCode indicating the success of the commission.
     */
    ReturnCode on_commission(
            const std::unordered_map<WorkerInfo, std::vector<std::string>, WorkerInfoHash>& workers);
    
    /**
     * @brief This method is called when receiving a "pufferscale_provider_decommission"
     * RPC from a Controller instance. The Master will order Workers to do the necessary
     * migrations. 
     *
     * @param workers Mapping of Worker to list of providers to decommission.
     *
	 * @return A ReturnCode indicating the success of the decommission.
     */
    ReturnCode on_provider_decommission(
            const std::unordered_map<WorkerInfo, std::vector<ProviderInfo>, WorkerInfoHash>& workers);

    /**
     * @brief This method is called when receiving a "pufferscale_decommission"
     * RPC from a Controller instance. The Master will order Workers to decommission
	 * all providers present on the specified workers.
     *
     * @param workers Information on the workers to decommission.
     *
	 * @return A ReturnCode indicating the success of the decommission.
     */
    ReturnCode on_decommission(
            const std::vector<WorkerInfo>& workers);
    
    /**
     * @brief This method is called when receiving a "pufferscale_list_all_buckets"
     * RPC from a Controller instance. The Master will return to the Controller a
     * map of all workers and their managed buckets.
     *
     * @param req Thallium request
	 * @param buff_size Size of the buffer to return the information.
	 * @param remote_bulk Bulk of size buff_size to store the information to return.
     */
    void on_list_all_buckets(
			const tl::request& req, 
			size_t buff_size, 
			tl::bulk remote_bulk,
			int request_id);

    /**
     * @brief This method will be called when the engine is finalized. 
     * It clears up all thallium buckets such as endpoint, remote_procedure,
     * requests, mutexes, etc.
     */
    void on_finalize();

	/**
	 * @brief Send the migration plans to all workers for them
	 * to migrate buckets accordingly.
	 *
	 * @param migrationPlans map of MigrationPlan for each Worker
	 *
	 * @return A ReturnCode indicating the success of the migration.
	 */
	ReturnCode send_migration_plan(
					std::unordered_map<WorkerInfo,
							MigrationPlan,
							WorkerInfoHash>& migrationPlans,
							int parallel_transfers);

	/**
	 * @brief Internal method that list the providers of each type
	 * that are being rescaled and start the rebalancing
	 *
	 * @param commissioned_providers Mapping of Workers to commissioned providers
	 * @param decommissioned_providers Mapping of Workers to decommissioned providers
	 * @param decommission_each_node Whether to decommission all providers on Workers
	 * that have an entry in decommissioned_providers
	 *
	 * @return A ReturnCode indicating the success of the migration
	 */ 
	ReturnCode rebalancing(
            const std::unordered_map<WorkerInfo, 
					std::unordered_map<std::string,int>, 
					WorkerInfoHash>& commissioned_providers,
            const std::unordered_map<WorkerInfo, 
					std::vector<ProviderInfo>, 
					WorkerInfoHash>& decommissioned_providers,
			bool decommission_each_node);


	/**
	 * @brief Rebalance the data for one provider type
	 *
	 * @param service_name Type of the provider being rebalanced
	 * @param commissioned_providers Providers being commissioned
	 * @param decommissioned_providers Providers being decommissioned
	 * @param other_providers Providers of the same type staying in the cluster
	 * @param workers Buckets available on each worker
	 *
	 * @return A ReturnCode indicating the success of the migration
	 */
	ReturnCode rebalance_providers(
			const std::string& service_name,
            const std::unordered_map<WorkerInfo, 
					int, 
					WorkerInfoHash>& commissioned_providers,
            const std::unordered_map<WorkerInfo, 
					std::vector<ProviderInfo>, 
					WorkerInfoHash>& decommissioned_providers,
            const std::vector<WorkerInfo>& hosts, 
			const std::unordered_map<WorkerInfo, 
					WorkerQuotas, 
					WorkerInfoHash>& workers,
			const ProviderRescalingParameters prp);

    public:

    /**
     * @brief Constructor.
     *
     * @param e thallium engine to use (must be set as a server)
     * @param provider_id provider id to use for this Master
     */
    Master(tl::engine& e, uint16_t provider_id=1);

    /**
     * @brief Destructor. This destructor will wait for the thallium
     * engine to be finalized before returning.
     */
    ~Master();

	/**
	 * @brief Configure the parameters of the scheduling algorithm 
	 * for a provider.
	 *
	 * The function overwrites the previous parameters if set.
	 *
	 * @param service_name Name of the provider for which the 
	 * parameters are set.
	 * @param weight_load How important is load balancing. Must be
	 * strictly positive, the lower the weight, the more load-balancing 
	 * is considered.
	 * @param weight_data How important is data balancing. Must be
	 * strictly positive, the lower the weight, the more data-balancing 
	 * is considered. This parameter is important to have stable 
	 * rescaling durations.
	 * @param weight_transfers How important is the speed of the rescaling
	 * operation. Must be strictly positive, the lower the weight, 
	 * the faster the operation.
	 * @param use_disk Whether the provider has data on disk.
	 * @param use_memory Whether the provider store significant data
	 * in memory. (at least one of use_disk and use_memory must be true)
	 * @param parallel_migrations Number of migrations that can be
	 * started in parallel to transfer data out of a provider.
	 *
	 * @return False if the weights have invalid values.
	 */
	bool configure_provider(
			const std::string& service_name,
			double weight_load,
			double weight_data,
			double weight_transfers,
			bool use_disk,
			bool use_memory,
			int parallel_migrations);
	/**
	 * Returns the duration of the last scheduling phase without the transfers.
	 *
	 * @return duration of the scheduling phase
	 */
	double get_duration_scheduling();
};

}

#endif
