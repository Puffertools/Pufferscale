/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_WORKER_H
#define __PUFFERSCALE_WORKER_H

#include <unordered_map>
#include <vector>
#include <utility>
#include <string>
#include <chrono>

#include <thallium.hpp>
#include <thallium/serialization/stl/unordered_map.hpp>
#include <thallium/serialization/stl/vector.hpp>
#include <thallium/serialization/stl/pair.hpp>

#include <pufferscale/Callbacks.hpp>
#include <pufferscale/MasterInfo.hpp>
#include <pufferscale/MigrationPlan.hpp>
#include <pufferscale/ProviderInfo.hpp>
#include <pufferscale/ProviderMetadata.hpp>
#include <pufferscale/BucketInfo.hpp>
#include <pufferscale/BucketMetadata.hpp>
#include <pufferscale/BucketTag.hpp>
#include <pufferscale/ReturnCode.hpp>
#include <pufferscale/WorkerInfo.hpp>
#include <pufferscale/WorkerMetadata.hpp>

namespace tl = thallium;

namespace pufferscale {

class Worker : public tl::provider<Worker> {
private:
	/// Information about a bucket 
	struct BucketData {
		/// Callback for the migration of the buckets
		MigrationCallback	 	m_migrationCallback;
		/// Callback for the update of the metadata of the bucket
		MetadataUpdateCallback 	m_metadataUpdateCallback;
		/// BucketInfo
		BucketInfo 			m_bucket_info;
		/// uargs of the bucket
		void*					m_uargs;
	};

	/// Information about a managed provider
	struct ProviderData {
		/// Metadata of the provider
		ProviderMetadata 		m_provider_metadata;
		/// Hosted buckets
		std::unordered_map<BucketTag, 
							BucketData, 
							BucketTagHash> 	m_buckets;
		/// uargs of the provider
		void* 					m_provider_uargs;
		
		bool 					m_in_rescaling = false;
		/// is in decommission
		bool 					m_in_decommission = false;
	};


	/// Information about a type of providers
	/// Set at join time
	struct ProviderManagementData {
		/// Provider name
		std::string 				m_service_name;
		/// Callback to start a provider
		InitiateProviderCallback 	m_initiate;
		/// Callback to terminate a provider
		TerminateProviderCallback 	m_terminate;
		/// uargs for the creation and termination of the provider
		void* 						m_constructor_uargs;

		/// Callback for the prerescaling step
		PreRescalingCallback		m_prerescaling;
		/// Callback for the postrescaling step
		PostRescalingCallback		m_postrescaling;
	};

	/// Buckets managed by this worker
	std::unordered_map<ProviderInfo, 
						ProviderData, 
						ProviderInfoHash> m_managed_buckets; 
	/// Mutex on m_managed_buckets
	std::unique_ptr<tl::mutex> m_buckets_mtx;

	/// Providers that can run of this Worker
	std::unordered_map<std::string, 
						ProviderManagementData> m_provider_management;

	/// RPC to Master to join the cluster
    tl::remote_procedure 	m_join;
	/// RPC to Master to leave the cluster
    tl::remote_procedure 	m_leave;

	/// Information about this worker
	WorkerInfo 				m_this_workerInfo;
	/// Metadata of this worker
	WorkerMetadata 			m_this_workerMetadata;
	/// Information about the master
	MasterInfo 				m_masterInfo;

	/// RequestID of the last metadata_update
	int m_last_metadata_update = -1;
	/// if true, m_last_metadata_update should not be used
	bool m_need_metadata_update = true;

	/// Indicate whether the worker has joined the cluster
	bool m_has_joined = false;

	/**
	 * @brief Update the metadata of al managed buckets.
	 */
	void update_metadata(int request_id);


    /**
     * @brief This method is called when receiving a "pufferscale_migrate"
     * RPC from the Master. The MigrationPlan consists of a sequence of
     * MigrationOperations that have to be executed. Each MigrationOperation
     * references a bucket managed by this Worker, and a target provider.
     * This operation sends back the the Master a vector of BucketInfo
     * representing the location and identification information of the newly
     * migrated buckets.
     *
     * @param migrationPlan Migration plan
     * @param max_concurrent_transfers Number of transfers that can happen 
	 * in parallel
	 *
     * @return true if all planned migration have been completed.
     */
    ReturnCode on_migrate(
					const MigrationPlan& migrationPlan, 
					int max_concurrent_transfers);
    
	/**
	 * @brief Method called upon reception of a "pufferscale_list_all_buckets"
	 * Return to the master the list of buckets managed by the worker if the
	 * size of the remote_bulk is sufficient.
	 * If not, return the size needed to contain the information.
	 *
	 * Return a std::vector<BucketInfo>.
	 *
	 * @param req Thallium request.
	 * @param buff_size Size of the remote_bulk to store data.
	 * @param remote_bulk Bulk to use to return data.
	 */
	void on_list_all_buckets(const tl::request& req, 
								size_t buff_size, 
								tl::bulk remote_bulk,
								int request_id);

	/**
	 * @brief Method called upon reception of a 
	 * "pufferscale_list_provider_buckets"
	 * Return to the master the list of buckets from providers 
	 * named service_name and managed by the worker if the size of 
	 * the remote_bulk is sufficient.
	 * If not, return the size needed to contain the information.
	 *
	 * Return a std::vector<ProviderSnapshot>.
	 *
	 * @param req Thallium request.
	 * @param provider Provider for which the data is requested 
	 * @param buff_size Size of the remote_bulk to store data.
	 * @param remote_bulk Bulk to use to return data.
	 */
	void on_list_provider_buckets(const tl::request& req, 
								const std::string& service_name,
								double weight_load,
								double weight_data,
								size_t buff_size, 
								tl::bulk remote_bulk,
								int request_id);
	/**
	 * @brief Method called upon reception of a "pufferscale_list_providers"
	 * Return to the master the list of providers managed by the worker 
	 * and their load and amount of data if the size of the remote_bulk is 
	 * sufficient. 
	 * If not, return the size needed to contain the information.
	 *
	 * Return a std::unordered_map<ProviderInfo,ProviderMetadata,
	 * ProviderInfoHash>. 
	 *
	 * @param req Thallium request.
	 * @param buff_size Size of the remote_bulk to store data.
	 * @param remote_bulk Bulk to use to return data.
	 */
	void on_list_providers(const tl::request& req, 
								size_t buff_size, 
								tl::bulk remote_bulk,
								int request_id);

	/**
	 * @brief Mark the provider as being rescaled, spawn the requested 
	 * new providers, forbid the management of new buckets on 
	 * decommissioned providers
	 *
	 * @param service_name Name of the provider to consider
	 * @param to_commission Number of providers to commission
	 * @param to_decommission ProviderInfo of the providers to decommission
	 *
	 * @return ReturnCode indicating the success of the operation
	 */
	ReturnCode on_initiate_rescaling(const std::string& service_name,
				int to_commission,
				const std::vector<ProviderInfo>& to_decommission);

	/**
	 * @brief Unlock the providers, delete the decommissioned providers
	 * if requiered.
	 *
	 * @param service_name Name of the provider to consider
	 * @param remove_in_decommission Whether to delete the providers that 
	 * are in decommission
	 *
	 * @return ReturnCode indicating the success of the operation
	 */
	ReturnCode on_terminate_rescaling(const std::string& service_name,
				const bool remove_in_decommission);

	/**
     * @brief This method will be called when the engine is finalized. 
     * It clears up all thallium buckets such as endpoint, remote_procedure,
     * requests, mutexes, etc.
     */
    void on_finalize();

    public:

    /**
     * @brief Constructor.
     *
     * @param e thallium engine, but be initialized as server
     * @param provider_id provider id
     */
    Worker(tl::engine& e, uint16_t provider_id=1);

    /**
     * @brief Destructor.
     */
    ~Worker();

	/**
	 * @brief Set a provider that is initially on the node.
	 * Provider type must be configured before.
	 *
	 * Can only be used before joining the master.
	 *
	 * @param provider ProviderInfo about the provider
	 *
	 * @return true if the provider was added
	 */
	bool manages_provider(const ProviderInfo& provider);

    /**
     * @brief Sets the buckets that this Worker manages.
     *
	 * Cannot be used when the node is being decommissioned.
	 *
	 * This method must also be called after the migration of 
	 * the bucket so that the new host is informed of its presence.
	 *
     * @param provider Provider hosting the buckets
	 * @param bucket_tag Buckets managed by this Provider
     * @param migrationCallback Callback to call when the Master 
     *   requests migration of this bucket.
     * @param metadataUpdateCallback Callback to call when the Master
	 * 	 requests an update of the metadata of the bucket.
	 * @param uarg Argument to pass to the migrationCallback and 
	 * MetadataUpdateCallback.
	 *
	 * @return false if no bucket can be added at the moment.
	 */
    bool manages(
			const ProviderInfo& provider,
            const BucketTag& bucket_tag,
            const MigrationCallback& migrationCallback,
			const MetadataUpdateCallback& metadataUpdateCallback,
			void* uarg);
   
    /**
     * @brief Remove a bucket from the the worker's management.
     *
	 * Cannot be used during a rescaling operation.
	 *
	 * @param provider Provider that host the bucket
	 * @param bucket_tag Bucket to stop managing
	 *
	 * @return false if no bucket can be removed at the moment.
	 */
	bool stop_managing(
			const ProviderInfo& provider,
            const BucketTag& bucket_tag);

	/**
     * @brief Makes this Worker join the Master.
     *
     * @param master Information on how to contact the Master.
     *
     * @return true if the Worker successfully joined the Master, 
	 * false otherwise.
     */
    bool join(const MasterInfo& master);

	/**
     * @brief Makes this Worker leave the cluster and shutdown all managed 
	 * providers.
     *
	 * Cannot be used during a rescaling operation.
	 *
     * @return true if the Worker successfully left, false otherwise.
     */
	bool leave();

	/**
	 * @brief Sets the metadata about the worker
	 * 
	 * @param memory_capacity Available space in memory for the buckets
	 *   managed by Pufferscale.
	 * @param network_bandwidth Network bandwidth of the worker.
	 *
	 * @return true or raise an exception in case of invalid parameters.
	 * Returns false if the configuration cannot be changed anymore.
	 */
	bool configure_worker(double memory_capacity, double network_bandwidth);

	/**
	 * @brief Sets the metadata about the persistent storage of the worker
	 * 
	 * @param capacity Available storage space for the buckets managed by 
	 * Pufferscale.
	 * @param read_bw Aggregated read bandwidth of the persistent storage.
	 * @param write_bw Aggregated write bandwidth of the persistent storage.
	 *
	 * @return true or raise an exception in case of invalid parameters.
	 * Returns false if the configuration cannot be changed anymore.
	 */
	bool configure_disk(double capacity, double read_bw, double write_bw);
	

	/** 
	 * @brief Register a type of provider defined by its name.
	 * It includes how to start it, stop it.
	 *
	 * @param service_name Name of the type of provider
	 * @param ipc InitiateProviderCallback to start a provider (mendatory)
	 * @param tpc TerminateProviderCallback to terminate a provider (mendatory)
	 * @param pre PreRescalingCallback that is called before a rescaling 
	 * (optional)
	 * @param post PostRescalingCallback that is called after a rescaling 
	 * (optional)
	 * @param p_uargs Arguments to pass to ipc and tpc when called
	 */
	void register_provider(	const std::string& service_name,
							void* p_uargs,
							const InitiateProviderCallback& ipc, 
							const TerminateProviderCallback& tpc, 
							const PreRescalingCallback& pre
								= PreRescalingCallback(),
							const PostRescalingCallback& post
								= PostRescalingCallback());
};

}

#endif
