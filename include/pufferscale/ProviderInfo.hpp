/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_PROVIDER_INFO_H
#define __PUFFERSCALE_PROVIDER_INFO_H

#include <iostream>
#include <string>
#include <functional>

#include <thallium.hpp>
#include <thallium/serialization/stl/string.hpp>

#include <pufferscale/Identifiers.hpp>

namespace pufferscale {

/**
 * Information about a single provider.
 */


struct ProviderInfo {

	/// ID of the provider (to distinguish multiple providers of 
	/// the same service managed by the same worker) 
    ProviderID  m_provider_id;
	/// Name of the service the provider is part of
    std::string m_service_name;
	/// Address of the provider
    std::string m_provider_address;

    template<typename A>
    void serialize(A& ar) {
        ar & m_provider_id;
        ar & m_service_name;
        ar & m_provider_address;
	}

	bool operator  == (const ProviderInfo& pi) const{
		return m_provider_id == pi.m_provider_id
				&& m_service_name == pi.m_service_name
				&& m_provider_address == pi.m_provider_address;
	}

	friend std::ostream& operator<<(std::ostream& os, const ProviderInfo& pi){
		os << "ProviderInfo[" << pi.m_service_name << ", " << pi.m_provider_address;
		os << ", " << pi.m_provider_id << "]";
		return os;
	}
};	


struct ProviderInfoHash {

    typedef std::size_t result_type;

    std::hash<ProviderID>  m_h1;
    std::hash<std::string>  m_h2;

    result_type operator()(const ProviderInfo& p) const {
        return m_h1(p.m_provider_id) ^ m_h2(p.m_service_name) ^ m_h2(p.m_provider_address) ;
    }
};


}

#endif
