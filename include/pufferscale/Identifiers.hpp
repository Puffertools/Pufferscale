/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_IDENTIFIERS_H
#define __PUFFERSCALE_IDENTIFIERS_H

namespace pufferscale {

typedef std::uint64_t BucketID;
typedef std::uint16_t ProviderID;

}

#endif
