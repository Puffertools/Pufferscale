/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_BUCKET_TAG_H
#define __PUFFERSCALE_BUCKET_TAG_H

#include <string>

#include <thallium.hpp>
#include <thallium/serialization/stl/string.hpp>

#include <pufferscale/Identifiers.hpp>


/**
 * Static info about a bucket (also called a bucket in related publications)
 */

namespace pufferscale {

struct BucketTag {

	/// Name of the provider that manages it
    std::string             m_service_name;
	/// Id of the bucket
    BucketID 				m_bucket_id;

    template<typename A>
    void serialize(A& ar) {
        ar & m_service_name;
        ar & m_bucket_id;
	}

	bool operator == (const BucketTag& r2) const{
		return m_service_name == r2.m_service_name
				&& m_bucket_id == r2.m_bucket_id;
	};
	
	friend std::ostream& operator<<(std::ostream& os, const BucketTag& ri){
		os << "BucketInfo[" << ri.m_service_name << "," << ri.m_bucket_id << "]";
		return os;
	}	
};
	

struct BucketTagHash {

    typedef std::size_t result_type;

	static std::hash<std::string> m_h1;
	static std::hash<BucketID>  m_h2;

    result_type operator()(const BucketTag& r) const {
        return m_h1(r.m_service_name) ^ m_h2(r.m_bucket_id);
    }
};
}

#endif

