/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __CALLBACKS_H
#define __CALLBACKS_H

#include <functional>
#include <pufferscale/BucketTag.hpp>
#include <pufferscale/BucketMetadata.hpp>
#include <pufferscale/MigrationPlan.hpp>

namespace pufferscale {

/**
 * @brief Provide the metadata of a bucket
 *
 * @param tag BucketTag of the data for which the metadata is requested
 * @param uargs Uargs set when the bucket was declared to the worker
 *
 * @return Metadata of the bucket
 */
typedef std::function<BucketMetadata(const BucketTag&, void*)> MetadataUpdateCallback;

/**
 * @brief Migrate a bucket
 *
 * WARNING: Upon reception of the bucket, the bucket MUST be registered once 
 * again on the destination using Worker::manages. If not done, the bucket is simply
 * not managed by Pufferscale anymore.
 *
 * @param mop MigrationOperation with the informations about the transfer
 * @param uargs Uargs set when the bucket was declared to the worker
 *
 * @return true if the migration was successful
 */
typedef std::function<bool(const MigrationOperation&, void*)> MigrationCallback;

/// Indicate the success of the initialization of a provider
struct ProviderInitializationData {
	/// True if successful
	bool 			m_success = true;
	/// ProviderInfo of the newly created provider
	ProviderInfo 	m_provider_info;
	/// Args to pass to the terminate callback 
	void*			m_provider_uargs = nullptr;
};


/**
 * @brief Terminate a provider
 *
 * @param ProviderInfo provider to terminate
 * @param Uargs provided at the initialization of the provider
 * @param Uargs declared when setting the callback
 *
 * @return true if the termination was successful
 */
typedef std::function<bool(const ProviderInfo&, void*, void*)> TerminateProviderCallback;

/**
 * @brief Terminate a provider
 *
 * @param Uargs declared when setting the callback
 *
 * @return ProviderInitializationData 
 */
typedef std::function<ProviderInitializationData(void*)> InitiateProviderCallback;

/**
 * @brief Function called before a rescaling operation on all providers
 *
 * @param Uargs declared when setting the callback
 */
typedef std::function<void(void*)> PreRescalingCallback;

/**
 * @brief Function called after a rescaling operation on all providers
 *
 * @param Uargs declared when setting the callback
 */
typedef std::function<void(void*)> PostRescalingCallback;

}
#endif

