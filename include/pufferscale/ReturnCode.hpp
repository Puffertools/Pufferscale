/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_RETURN_CODE_H
#define __PUFFERSCALE_RETURN_CODE_H

#include <thallium.hpp>

#include <vector>

#include <thallium/serialization/stl/vector.hpp>

#include <pufferscale/WorkerInfo.hpp>
#include <pufferscale/BucketTag.hpp>
#include <pufferscale/ProviderInfo.hpp>
#include "pufferscale/PufferscaleExceptions.hpp"


/**
 * Structure used to return error information between 
 * the components of Pufferscale.
 */

namespace pufferscale {

enum error_codes: int32_t {
	ALL_GOOD = 0,
	
	/// UnknownWorkerException(worker)
	/// Worker is not known by the master
	UNKNOWN_WORKER,

	/// AlreadyKnownWorkerException(worker) 
	/// Worker has already joined
	ALREADY_KNOWN_WORKER,

	/// ProviderAlreadyPresentException(worker,provider)
	/// Provider has already been commissioned
	PROVIDER_PRESENT,

	/// ProviderConstraintsException(worker,provider)
	/// The constraints for this provider are not satisfied by the worker
	PROVIDER_CONSTRAINTS,
	
	/// ProviderNotPresentException(worker,provider)
	/// The provider is not managed by the worker
	UNKNOWN_PROVIDER,

	/// NoProviderToMoveBucketException(bucket, worker)
	/// No provider is available to place a bucket
	NO_AVAILABLE_PROVIDER,
	
	/// UnknownBucketException(worker,bucket)
	/// Bucket is not managed by the worker
	UNKNOWN_BUCKET,

	/// BucketNotMigratedException (worker,bucket)
	/// Bucket could not be migrated
	BUCKET_NOT_MIGRATED,
	
	/// ProviderCannotBeDecommissionedException(worker,provider)  
	/// There are buckets left on the provider and it can't be decommissioned
	BUCKETS_ON_PROVIDER,
	
	/// ProviderTypeUnknownException (worker,service_name)
	/// No callbacks set for this type of provider
	PROVIDER_TYPE_UNKNOWN,
	
	/// TooMuchDataException (service_name)
	/// There are too much data on the cluster for this rebalancing
	TOO_MUCH_DATA,
	
	/// ProviderNotCommissionedException (worker_info, service_name)
	/// Could not start the provider
	PROVIDER_NOT_COMMISSIONED,

	/// A rescaling operation is taking place
	/// Should never happen user-side
	OPERATION_RUNNING,

	/// Buffer is too small to return data
	/// Should never happen user-side
	BUFFER_SIZE,

	/// NoDiskException (worker_info, service_name)
	/// Provider needs a disk, Worker does not have one
	NO_DISK
};


struct ReturnCode {
	int32_t m_error_code = error_codes::ALL_GOOD;
	WorkerInfo m_worker;
	ProviderInfo m_provider;
	BucketTag m_bucket;
	std::string m_service_name;
	bool m_multiple_errors = false;

    template<typename A>
    void serialize(A& ar) {
			ar & m_error_code;
			ar & m_worker;
			ar & m_provider;
			ar & m_bucket;
			ar & m_multiple_errors;
		}
	
	bool interpret() const{
		if (m_error_code == error_codes::ALL_GOOD){
			return true;
		}
		
		if (m_error_code == error_codes::UNKNOWN_WORKER)	
			std::cout << UnknownWorkerException(m_worker).what() << std::endl;
		
		else if (m_error_code == error_codes::ALREADY_KNOWN_WORKER)
			std::cout << AlreadyKnownWorkerException(m_worker).what() << std::endl; 
		
		else if (m_error_code == error_codes::PROVIDER_PRESENT)	
			std::cout << ProviderAlreadyPresentException(m_worker,m_provider).what() << std::endl;
		
		else if (m_error_code == error_codes::PROVIDER_CONSTRAINTS)
			std::cout << ProviderConstraintsException(m_worker,m_provider).what() << std::endl;
		
		else if (m_error_code == error_codes::UNKNOWN_PROVIDER)
			std::cout << ProviderNotPresentException(m_worker,m_provider).what() << std::endl; 
		
		else if (m_error_code == error_codes::NO_AVAILABLE_PROVIDER)
			std::cout << NoProviderToMoveBucketException(m_bucket, m_worker).what() << std::endl;
		
		else if (m_error_code == error_codes::UNKNOWN_BUCKET)
			std::cout << UnknownBucketException(m_worker, m_bucket).what() << std::endl;
		
		else if (m_error_code == error_codes::BUCKET_NOT_MIGRATED)
			std::cout << BucketNotMigratedException(m_worker, m_bucket).what() << std::endl;
		
		else if (m_error_code == error_codes::BUCKETS_ON_PROVIDER)
			std::cout << ProviderCannotBeDecommissionedException(m_worker, m_provider).what() << std::endl;	
		
		else if (m_error_code == error_codes::PROVIDER_TYPE_UNKNOWN)
			std::cout << ProviderTypeUnknownException(m_worker,m_service_name).what() << std::endl;	
		
		else if (m_error_code == error_codes::TOO_MUCH_DATA)
			std::cout << TooMuchDataException(m_service_name).what() << std::endl;	
		
		else if (m_error_code == error_codes::PROVIDER_NOT_COMMISSIONED)
			std::cout << ProviderNotCommissionedException(m_worker,m_service_name).what() << std::endl;	
	
		else if (m_error_code == error_codes::NO_DISK)
			std::cout << NoDiskException(m_worker,m_service_name).what() << std::endl;

		else std::cout << "Error code " << m_error_code << std::endl;
	
		return false;
	}

	bool operator !(){
		return m_error_code != error_codes::ALL_GOOD;
	}
	operator bool() const{
		return m_error_code == error_codes::ALL_GOOD;
	}
};


}



#endif
