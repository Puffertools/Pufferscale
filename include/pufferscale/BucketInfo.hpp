/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __PUFFERSCALE_BUCKET_INFO_H
#define __PUFFERSCALE_BUCKET_INFO_H

#include <thallium.hpp>
#include <thallium/serialization/stl/string.hpp>

#include <pufferscale/BucketTag.hpp>
#include <pufferscale/ProviderInfo.hpp>
#include <pufferscale/BucketMetadata.hpp>


/**
 * All Info about a bucket (Static, placement, metadata)
 */

namespace pufferscale {

struct BucketInfo {

	/// Identifier of the bucket
	BucketTag 			m_bucket_tag;
	/// Identifier of its host
    ProviderInfo            m_provider_info;
	/// Metadata of the bucket
	BucketMetadata		m_bucket_metadata;

    template<typename A>
    void serialize(A& ar) {
        ar & m_bucket_tag;
        ar & m_provider_info;
		ar & m_bucket_metadata;
	}

	bool operator == (const BucketInfo& r2) const{
		return m_provider_info == r2.m_provider_info
				&& m_bucket_tag == r2.m_bucket_tag;
	};
	
	friend std::ostream& operator<<(std::ostream& os, const BucketInfo& ri){
		os << "BucketInfo[" << ri.m_bucket_tag << "," << ri.m_provider_info;
		os << "," << ri.m_bucket_metadata << "]";
		return os;
	}	
};
}

#endif
