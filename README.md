# Pufferscale

Pufferscale is a rescaling service developed in the context of the [Mochi project](http://www.mcs.anl.gov/research/projects/mochi/).
Pufferscale is a library designed to manage data units (buckets) of behalf of microservices during rescaling operations (adding or removing nodes).
It tracks the position of each bucket in the system, and during rescaling operations Pufferscale determine the new position of each bucket (moved out of removed nodes, added to newly added nodes to balance the load) and give the transfer instructions to the microservices.

![Structure of Pufferscale](doc/Pufferscale.png)

Pufferscale leverage these rescaling operation to also balance the load across the cluster to avoid hotspots.
However, to ensure fast rescaling operations on the long term, Pufferscale also balance the amount of data per node.

**Pufferscale is designed to manage buckets in a context without data replication. Using it when replication is used is inefficient since smarter mechanisms should be used.**

# How to build and install

## Dependencies 

Pufferscale depends on the following:
1. [thallium](https://xgitlab.cels.anl.gov/sds/thallium)

## Building and installing

```
mkdir build
cd build
cmake .. 
make
make install
```

## Using spack to install Pufferscale 

[Spack](https://spack.io/) can be used to install Pufferbench and its dependencies.

```
git clone git@gitlab.inria.fr:Puffertools/Pufferspack.git
spack repo add Pufferspack
spack install Pufferscale
```

