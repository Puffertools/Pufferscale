/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <iostream>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <limits>
#include <unordered_set>

#include <thallium/serialization/stl/pair.hpp>
#include <thallium/serialization/stl/vector.hpp>
#include <thallium/serialization/stl/unordered_map.hpp>

#include "pufferscale/Master.hpp"
#include "pufferscale/PufferscaleExceptions.hpp"
#include "pufferscale/ProviderMetadata.hpp"

#define MARK std::cout << __FILE__<< ":" << __LINE__ << std::endl;
 
namespace pufferscale {

Master::Master(tl::engine& e, uint16_t provider_id)
: tl::provider<Master>(e,provider_id)
/* RPC this Master object can send */
, m_migrate(e.define("pufferscale_migrate"))
, m_list_all_buckets(e.define("pufferscale_list_all_buckets"))
, m_list_provider_buckets(e.define("pufferscale_list_provider_buckets"))
, m_list_providers(e.define("pufferscale_list_providers"))
, m_initiate_rescaling(e.define("pufferscale_initiate_rescaling")) 
, m_terminate_rescaling(e.define("pufferscale_terminate_rescaling")){ 
    /* RPC this Master object can receive */
    define("pufferscale_join",                  &Master::on_join);
    define("pufferscale_leave",                 &Master::on_leave);
    define("pufferscale_commission",            &Master::on_commission);
    define("pufferscale_decommission",          &Master::on_decommission);
    define("pufferscale_provider_decommission", &Master::on_provider_decommission);
    define("pufferscale_list_all_buckets_master",&Master::on_list_all_buckets);
    define("pufferscale_list_all_providers_master",&Master::on_list_providers);
    /* Set on_finalize callback */
    e.push_finalize_callback([this]() { this->on_finalize(); });

	m_op_mtx = std::unique_ptr<tl::mutex>(new tl::mutex);
	m_workers_mtx = std::unique_ptr<tl::mutex>(new tl::mutex);
	m_all_providers_mtx = std::unique_ptr<tl::mutex>(new tl::mutex);
	m_all_buckets_mtx = std::unique_ptr<tl::mutex>(new tl::mutex);
	m_next_token_mtx = std::unique_ptr<tl::mutex>(new tl::mutex);
}

Master::~Master() {}

ReturnCode Master::on_join(
        const WorkerInfo& workerInfo,
        const WorkerMetadata& workerMetadata) {

	m_op_mtx->lock();
	m_workers_mtx->lock();
	if (m_workers.count(workerInfo) == 1){
		m_workers_mtx->unlock();
		std::cout << AlreadyKnownWorkerException(workerInfo).what() << std::endl;
		ReturnCode rc;
		rc.m_error_code = error_codes::ALREADY_KNOWN_WORKER;
		rc.m_worker = workerInfo;
		m_op_mtx->unlock();
		return rc;
	} 
    /*
	std::cout << "Worker " << workerInfo << " with " << workerMetadata;
	std::cout << " is joining the pool of buckets."<< std::endl;
	//*/
	
	// Emplacing the worker in the list
	tl::endpoint workerEndpoint = get_engine().lookup(workerInfo.m_address);
	m_workers[workerInfo].m_handle = 
		std::shared_ptr<tl::provider_handle>(
			new tl::provider_handle(workerEndpoint, 
				workerInfo.m_provider_id));
	m_workers[workerInfo].m_worker_metadata = workerMetadata;
	m_workers_mtx->unlock();	
	m_op_mtx->unlock();
	return ReturnCode();
}

void Master::on_finalize() {
    std::cout << "Master finalize" << std::endl;
	m_op_mtx->lock();
	m_workers.clear();
    m_op_mtx->unlock();
	m_op_mtx.reset();
	m_workers_mtx.reset();
}

ReturnCode Master::send_migration_plan(
		std::unordered_map<WorkerInfo,
			MigrationPlan,WorkerInfoHash>& migrationPlans,
			int parallel_transfers){
	ReturnCode rc;
	
	std::vector<tl::async_response> requests;

	//std::cout << "Sending orders: start" << std::endl;

	std::unordered_map<WorkerInfo, MigrationPlan, WorkerInfoHash>::iterator it;
	for (it = migrationPlans.begin(); it != migrationPlans.end(); ++it){
		requests.push_back(m_migrate.on(*(m_workers[it->first].m_handle))
										.async(it->second,parallel_transfers));
	}
	
	//std::cout << "Sending orders: waiting" << std::endl;

	int ct = 0;
	for (tl::async_response& resp : requests){
		ct++;
		ReturnCode ret = resp.wait().as<ReturnCode>();
		//std::cout << "Sending orders: recv " << ct << "/" << requests.size() << std::endl;
		if (ret.m_error_code != error_codes::ALL_GOOD){
			
			ret.interpret();
			if (rc.m_error_code == error_codes::ALL_GOOD){
				rc = ret;
			} else {
				rc.m_multiple_errors = true;
			}
		}	
	}
	//std::cout << "Sending orders: end" << std::endl;

	return rc;
}

void Master::on_list_all_buckets(const tl::request& req, 
		size_t buff_size, tl::bulk remote_bulk, int request_id) {

	std::pair<ReturnCode,std::pair<size_t,int>> status;   

	m_all_buckets_mtx->lock();
	if (request_id == 0 || request_id != m_all_buckets_token){
		m_all_buckets = get_all_buckets();
	}
	m_all_buckets_mtx->unlock();
	
	tl::buffer buffer(0);
	tl::buffer_output_archive boa(buffer,get_engine());

	boa & m_all_buckets;
	
	// Data would not fit in bulk
	if (buffer.size() > buff_size){
		status.first.m_error_code = BUFFER_SIZE;
		status.second.first = buffer.size();
		m_next_token_mtx->lock();
		m_next_token++;
		if (m_next_token == 0){
			m_next_token++;
		}
		status.second.second = m_next_token;
		m_all_buckets_mtx->lock();
		m_all_buckets_token = m_next_token;
		m_all_buckets_mtx->unlock();
		m_next_token_mtx->unlock();
		req.respond(status);
		return ;
	}
	m_all_buckets_mtx->lock();
	m_all_buckets_token = 0;
	m_all_buckets_mtx->unlock();

	tl::endpoint ep = req.get_endpoint();

	std::vector<std::pair<void*,std::size_t>> segments(1);
	segments[0].first  = (void*)(buffer.data());
	segments[0].second = buffer.size();
	tl::bulk local = get_engine().expose(segments, tl::bulk_mode::read_write);

	remote_bulk.on(ep) << local;
	
	status.second.first = buffer.size();
	
	req.respond(status);
}


std::unordered_map<WorkerInfo,std::vector<BucketInfo>,
	WorkerInfoHash> Master::get_all_buckets(){

	std::unordered_map<WorkerInfo,std::vector<BucketInfo>,
		WorkerInfoHash> results;

	std::unordered_map<WorkerInfo, WorkerData, WorkerInfoHash>::iterator it;

	// TODO parallelise
	m_workers_mtx->lock();
	for (it = m_workers.begin(); it != m_workers.end(); ++it){
		m_next_token_mtx->lock();
		int request_id = m_next_token;
		m_next_token++;
		m_next_token_mtx->unlock();
		size_t buff_size = 1024;
		tl::buffer buff(buff_size);
		std::vector<std::pair<void*,std::size_t>> segments(1);
		segments[0].first  = (void*)(buff.data());
		segments[0].second = buff_size;

		tl::bulk myBulk = get_engine().expose(segments, tl::bulk_mode::read_write);

		std::pair<ReturnCode,size_t> status;
		status = m_list_all_buckets
			.on(*(it->second.m_handle))(buff_size,
					myBulk,
					request_id)
			.as<std::pair<ReturnCode,size_t>>();

		while (status.first.m_error_code == BUFFER_SIZE){
			// Will loop only if new buckets are added.
			buff_size = status.second;

			buff.resize(buff_size);

			segments[0].first  = (void*)(buff.data());
			segments[0].second = buff_size;
			myBulk = get_engine().expose(segments, tl::bulk_mode::read_write);

			status = m_list_all_buckets
				.on(*(it->second.m_handle))(buff_size,
						myBulk,
						request_id)
				.as<std::pair<ReturnCode,size_t>>();
		}
		if (status.first.m_error_code != ALL_GOOD){
			status.first.interpret();
			assert(false);
		}

		tl::buffer_input_archive boa(buff,get_engine());

		boa & (results[it->first]);
	}
	m_workers_mtx->unlock();
	return results;

}

std::unordered_map<ProviderInfo,ProviderSnapshot,ProviderInfoHash> 
	Master::get_all_provider_buckets(
			const std::string& service_name,
			double weight_load,
			double weight_data,
			const std::vector<WorkerInfo>& hosts){

	std::unordered_map<ProviderInfo,ProviderSnapshot,ProviderInfoHash> results;

	std::vector<ProviderSnapshot> snaps;
	
	m_workers_mtx->lock();
	for (const WorkerInfo& wi : hosts){
		m_next_token_mtx->lock();
		int request_id = m_next_token;
		m_next_token++;
		m_next_token_mtx->unlock();
		
		// TODO parallelise
		
		size_t buff_size = 1024;
		tl::buffer buff(buff_size);
		std::vector<std::pair<void*,std::size_t>> segments(1);
		segments[0].first  = (void*)(buff.data());
		segments[0].second = buff_size;
		
		tl::bulk myBulk = get_engine().expose(segments, tl::bulk_mode::read_write);
		
		std::pair<ReturnCode,size_t> status = 
			m_list_provider_buckets
				.on(*(m_workers[wi].m_handle))(
						service_name,
						weight_load,
						weight_data,
						buff_size,
						myBulk,
						request_id)
				.as<std::pair<ReturnCode,size_t>>();
	
		while (status.first.m_error_code == BUFFER_SIZE){
			// Will loop only if new buckets are added.
			buff_size = status.second;
	
			buff.resize(buff_size);
	
			segments[0].first  = (void*)(buff.data());
			segments[0].second = buff_size;
			myBulk = get_engine().expose(segments, tl::bulk_mode::read_write);
			
			status = m_list_provider_buckets
					.on(*(m_workers[wi].m_handle))(
							service_name,
							weight_load,
							weight_data,
							buff_size,
							myBulk,
							request_id)
					.as<std::pair<ReturnCode,size_t>>();
		}
		if (!status.first){
			status.first.interpret();
			assert(false);
		}
	
		tl::buffer_input_archive boa(buff,get_engine());
	
		boa & snaps;

		for (const ProviderSnapshot& snap : snaps){
			results[snap.m_provider_info] = snap;
		}
	}
	m_workers_mtx->unlock();
	//*/
	return results;
}

void Master::on_list_providers(const tl::request& req, 
		size_t buff_size, tl::bulk remote_bulk, int request_id) {
	
	std::pair<ReturnCode,std::pair<size_t,int>> status;

	m_all_providers_mtx->lock();
	if (request_id == 0 || request_id != m_all_providers_token){
		std::unordered_map<WorkerInfo,
			std::unordered_map<ProviderInfo,ProviderMetadata,ProviderInfoHash>,
			WorkerInfoHash> providers;
		providers = get_all_providers();

		std::unordered_map<WorkerInfo,
			std::unordered_map<ProviderInfo,ProviderMetadata,ProviderInfoHash>,
			WorkerInfoHash>::iterator it;
		
		m_all_providers.clear();
		for (it = providers.begin(); it != providers.end(); ++it){
			for (const std::pair<ProviderInfo, ProviderMetadata>& elem: it->second)
				m_all_providers[it->first].push_back(elem.first);
		}
	}
	m_all_providers_mtx->unlock();


	tl::buffer buffer(0);
	tl::buffer_output_archive boa(buffer,get_engine());

	boa & m_all_providers;
	
	// Data would not fit in bulk
	if (buffer.size() > buff_size){
		status.first.m_error_code = BUFFER_SIZE;
		status.second.first = buffer.size();
		m_next_token_mtx->lock();
		m_next_token++;
		if (m_next_token == 0){
			m_next_token++;
		}
		status.second.second = m_next_token;
		m_all_providers_mtx->lock();
		m_all_providers_token = m_next_token;
		m_all_providers_mtx->unlock();
		m_next_token_mtx->unlock();
		req.respond(status);
		return ;
	}
	m_all_providers_mtx->lock();
	m_all_providers_token = 0;
	m_all_providers_mtx->unlock();

	tl::endpoint ep = req.get_endpoint();

	std::vector<std::pair<void*,std::size_t>> segments(1);
	segments[0].first  = (void*)(buffer.data());
	segments[0].second = buffer.size();
	tl::bulk local = get_engine().expose(segments, tl::bulk_mode::read_write);

	remote_bulk.on(ep) << local;
	
	status.second.first = buffer.size();
	
	req.respond(status);
}

std::unordered_map<WorkerInfo,
	std::unordered_map<ProviderInfo,ProviderMetadata,ProviderInfoHash>,
	WorkerInfoHash> Master::get_all_providers(){

	// TODO parallelise
	std::unordered_map<WorkerInfo,
	std::unordered_map<ProviderInfo,ProviderMetadata,ProviderInfoHash>,
	WorkerInfoHash> results;

	std::unordered_map<WorkerInfo, WorkerData, WorkerInfoHash>::iterator it;

	m_workers_mtx->lock();

	for (it = m_workers.begin(); it != m_workers.end(); ++it){
		
		m_next_token_mtx->lock();
		int request_id = m_next_token;
		m_next_token++;
		m_next_token_mtx->unlock();

		size_t buff_size = 1024;
		tl::buffer buff(buff_size);
		std::vector<std::pair<void*,std::size_t>> segments(1);
		segments[0].first  = (void*)(buff.data());
		segments[0].second = buff_size;

		tl::bulk myBulk = get_engine().expose(segments, tl::bulk_mode::read_write);

		std::pair<ReturnCode,size_t> status;
		status = m_list_providers
			.on(*(it->second.m_handle))(buff_size,
					myBulk,
					request_id)
			.as<std::pair<ReturnCode,size_t>>();

		while (status.first.m_error_code == BUFFER_SIZE){
			// Will loop only if new buckets are added
			buff_size = status.second;

			buff.resize(buff_size);

			segments[0].first  = (void*)(buff.data());
			segments[0].second = buff_size;
			myBulk = get_engine().expose(segments, tl::bulk_mode::read_write);

			status = m_list_providers
				.on(*(it->second.m_handle))(buff_size,
						myBulk,
						request_id)
				.as<std::pair<ReturnCode,size_t>>();
		}
		if (!status.first){
			status.first.interpret();
			assert(false);
		}

		tl::buffer_input_archive boa(buff,get_engine());

		boa & (results[it->first]);
	}
	m_workers_mtx->unlock();
	return results;
}


ReturnCode Master::rebalancing(
            const std::unordered_map<WorkerInfo, 
						std::unordered_map<std::string,int>, 
						WorkerInfoHash>& commissioned_providers,
            const std::unordered_map<WorkerInfo, 
						std::vector<ProviderInfo>, 
						WorkerInfoHash>& decommissioned_providers,
			bool decommission_each_node){

	

	std::unordered_map<std::string, double> disk_data_per_service_name;
	std::unordered_map<std::string, double> mem_data_per_service_name;
	std::unordered_map<std::string, int> nb_providers_per_name;
	std::unordered_map<std::string, bool> rescaled_provider_type;

	// Decommissioned providers per worker per name
	std::unordered_map<std::string,
		std::unordered_map<WorkerInfo,
		std::vector<ProviderInfo>,
		WorkerInfoHash>> decom_per_name;

	// Commissioned providers per worker per name
	std::unordered_map<std::string,
		std::unordered_map<WorkerInfo,int,WorkerInfoHash>> com_per_name;

	// Other providers per worker per name
	std::unordered_map<std::string,
		std::unordered_set<WorkerInfo,WorkerInfoHash>> hosts_per_name;

	// List all providers
	std::unordered_map<WorkerInfo,
			std::unordered_map<ProviderInfo,ProviderMetadata,ProviderInfoHash>,
			WorkerInfoHash> all_providers;
	
	
	all_providers = get_all_providers();

	// Providers after rescaling
	std::unordered_map<WorkerInfo, std::unordered_map<std::string, int>, 
		WorkerInfoHash> providers_per_worker;


	std::unordered_map<WorkerInfo, 
		std::vector<ProviderInfo>, 
		WorkerInfoHash>::const_iterator it;
    if (decommission_each_node){
		// In case of decommission_each_node, list providers
		std::unordered_map<ProviderInfo, 
			ProviderMetadata,
			ProviderInfoHash>::iterator pit;
		for (it = decommissioned_providers.cbegin();
				it != decommissioned_providers.cend();
				++it){
			for (pit = all_providers[it->first].begin();
					pit != all_providers[it->first].end();
					++pit){
				// Add to the list of provider names that will be rebalanced
				disk_data_per_service_name[pit->first.m_service_name] 
					+= pit->second.m_data_on_disk;
				mem_data_per_service_name[pit->first.m_service_name] 
					+= pit->second.m_data_in_memory;
				rescaled_provider_type[pit->first.m_service_name] = true;
				// Store info about in decommission providers
				decom_per_name[pit->first.m_service_name][it->first]
					.push_back(pit->first);	
				providers_per_worker[it->first][pit->first.m_service_name]--;
				//std::cout << "Pufferscale Decommissioning " << pit->first << std::endl;
			}
		}	
	} else {
		// Check that all decommissioned providers exist
		for (it = decommissioned_providers.cbegin();
				it != decommissioned_providers.cend();
				++it){
			for (const ProviderInfo& pi : it->second){
				if (all_providers[it->first].count(pi) == 0){
					ReturnCode rc;
					rc.m_error_code = error_codes::UNKNOWN_PROVIDER; 
					rc.m_worker = it->first;
					rc.m_provider = pi;
					rc.interpret();
					return rc;
				}
				disk_data_per_service_name[pi.m_service_name] += 
						all_providers[it->first][pi].m_data_on_disk;
				mem_data_per_service_name[pi.m_service_name] += 
						all_providers[it->first][pi].m_data_in_memory;
				rescaled_provider_type[pi.m_service_name]=true;
				decom_per_name[pi.m_service_name][it->first].push_back(pi);
				providers_per_worker[it->first][pi.m_service_name]--;

				//std::cout << "Pufferscale Decommissioning " << pi << std::endl;
			}
		}
	}

	// No need to check for existence, they have not been started
    std::unordered_map<WorkerInfo,std::unordered_map<std::string,int>, 
		WorkerInfoHash>::const_iterator it2;
	std::unordered_map<std::string,int>::const_iterator it3;
	for (it2 = commissioned_providers.cbegin();
			it2 != commissioned_providers.cend();
			++it2){
		for (it3 = it2->second.cbegin(); it3 != it2->second.cend(); ++it3){
			const std::string& service_name = it3->first;
			nb_providers_per_name[service_name] += it3->second;
			rescaled_provider_type[service_name]=true;
			com_per_name[service_name][it2->first] = it3->second;

			// Adjust number of providers per name with the number 
			// of newly commissioned providers
			nb_providers_per_name[service_name] += it3->second;
			
			// List the new host as a host for the provider
			hosts_per_name[service_name].insert(it2->first);
			providers_per_worker[it2->first][service_name]++;
		}
	}


	// List already existing hosts for all providers
	std::unordered_map<WorkerInfo,
			std::unordered_map<ProviderInfo,ProviderMetadata,ProviderInfoHash>,
			WorkerInfoHash>::iterator wit;
	for (wit = all_providers.begin();
			wit != all_providers.end();
			++wit){
		std::unordered_map<ProviderInfo, 
			ProviderMetadata,ProviderInfoHash>::iterator pit;
		for (pit = wit->second.begin();
				pit != wit->second.end();
				++pit){
			// Record data on disk and memory
			const std::string& service_name = pit->first.m_service_name;
			disk_data_per_service_name[service_name] 
				+= pit->second.m_data_on_disk;
			mem_data_per_service_name[service_name] 
				+= pit->second.m_data_in_memory;
			nb_providers_per_name[service_name] ++;
			// List hosts
			hosts_per_name[service_name].insert(wit->first);
			providers_per_worker[wit->first][service_name]++;
		}
	}
	

	// Compute Worker metadata per service_name per WorkerInfo
	std::unordered_map<std::string,
		std::unordered_map<WorkerInfo, 
			WorkerQuotas, WorkerInfoHash>> quotas;
	// Record disk_data and mem_data per provider
	std::unordered_map<std::string, int>::iterator nb_it;
	for (nb_it = nb_providers_per_name.begin();
			nb_it != nb_providers_per_name.end();
			++nb_it){
		disk_data_per_service_name[nb_it->first] /= nb_it->second;
		mem_data_per_service_name[nb_it->first] /= nb_it->second;
	}
	
	// Count each provider at the end of the rescaling operation per Worker
	std::unordered_map<WorkerInfo, 
		std::unordered_map<std::string,int>, 
		WorkerInfoHash>::const_iterator cit;
	for (cit = providers_per_worker.cbegin();
			cit != providers_per_worker.cend();
			++cit){

		// Data expected by all providers at the end of the operation
		double total_expected_disk_data = 0;
		double total_expected_mem_data  = 0;

		std::unordered_map<std::string,int>::const_iterator csit;
		for (csit = cit->second.cbegin(); 
				csit != cit->second.cend(); 
				++csit){
			total_expected_disk_data 
				+= disk_data_per_service_name[csit->first]*csit->second;
			total_expected_mem_data  
				+= mem_data_per_service_name[csit->first]*csit->second;	
		}


		// for each service_name in rescaled_provider_type
		for (csit = cit->second.cbegin(); csit != cit->second.cend(); ++csit){
			const std::string& p_name = csit->first;

			if (rescaled_provider_type.count(p_name) > 0){
				// The provider is being rescaled generate quotas
				quotas[p_name][cit->first].m_bandwidth_network    =
					m_workers[cit->first].m_worker_metadata.m_bandwidth_network;
				quotas[p_name][cit->first].m_bandwidth_disk_read  = 
					m_workers[cit->first].m_worker_metadata.m_bandwidth_disk_read;
				quotas[p_name][cit->first].m_bandwidth_disk_write = 
					m_workers[cit->first].m_worker_metadata.m_bandwidth_disk_write;
				quotas[p_name][cit->first].m_quota_disk			  =
					csit->second
					* disk_data_per_service_name[p_name]
					* m_workers[cit->first].m_worker_metadata.m_capacity_disk
					/ total_expected_disk_data;
				quotas[p_name][cit->first].m_quota_memory 		  = 
					csit->second
					* mem_data_per_service_name[p_name]
					* m_workers[cit->first].m_worker_metadata.m_capacity_memory
					/ total_expected_mem_data;
			}
		}
	}
	

	// Run rebalancing for each service_name
	std::unordered_map<std::string, bool>::iterator sit;;
	for (sit = rescaled_provider_type.begin();
			sit != rescaled_provider_type.end();
			++sit){
		std::vector<WorkerInfo> hosts;
		hosts.insert(hosts.begin(), 
				hosts_per_name[sit->first].begin(), 
				hosts_per_name[sit->first].end());
		// Default parameters for the scheduling 
		ProviderRescalingParameters prp;
		if (provider_parameters.count(sit->first) > 0)
			prp = provider_parameters[sit->first];
			
		ReturnCode rc = rebalance_providers(
							sit->first,
							com_per_name[sit->first],
							decom_per_name[sit->first],
							hosts,
							quotas[sit->first],
							prp);
		if (!rc){
			return rc;
		}
	}
	
	return ReturnCode();
}
			
			
struct WorkerSchedulingData {
	WorkerInfo wi;
	
	// During rescaling
	double sent	 	= 0;
	double recv 	= 0;
	double read 	= 0;
	double written 	= 0;
	// Capacity usage
	double mem		= 0; 
	double disk 	= 0; 

	double init_disk = 0;
	double init_mem  = 0;

	// Static data
	double bandwidth_network = 0;
	double bandwidth_disk_read = 0;
	double bandwidth_disk_write = 0;
	double quota_disk = 0;
	double quota_memory = 0;	

	// Providers hosted at the end of the rescaling
	int final_hosted_providers = 0;
	// Providers hosted at the beginning of the rescaling
	int before_hosted_providers = 0;
};

struct ProviderSchedulingData {
	ProviderInfo pi;
	WorkerSchedulingData* host;
	
	// Initially
	double total_data_mem = 0;
	double total_data_disk = 0;
	double total_load = 0;
	double max_data_mem = 0;
	double max_data_disk = 0;
	double max_load = 0;


	// During rescaling
	double penalty = 0;
	double load = 0;
	double mem = 0;
	double disk = 0;

	// Targets
	double target_mem = 0;
	double target_disk = 0;
	double target_load = 0;
	// Before rescaling ideals
	double previous_ideal_mem = 0;
	double previous_ideal_disk = 0;
	double previous_ideal_load = 0;

	// Is it in decommission
	bool   in_decommission = false;
};

struct CompareBucketInfo {
	double c_load = 0;
	double c_disk = 0;
	double c_mem = 0;

	/**
	 * @param wl Weight for the load-balancing
	 * @param ws Weight for the data-balancing
	 * @param n_disk Average amount of data per disk
	 * @param n_mem Average amount of data in memory
	 * @param n_load Average load
	 */
	CompareBucketInfo(double wl, double ws, 
			double n_disk, double n_mem, double n_load){
		// Switch WL and WS as the lower the weight is the more interesting
		// the parameter is
		if (n_load > 0)
			c_load = ws * ws / n_load / n_load;
		if (n_disk > 0)
			c_disk = wl * wl / n_disk / n_disk;
		if (n_mem > 0)
			c_mem  = wl * wl / n_mem  / n_mem;
	}
	
	bool operator() (const BucketInfo& r1, const BucketInfo& r2){
		double n1 = r1.m_bucket_metadata.m_bucket_data_on_disk
					* r1.m_bucket_metadata.m_bucket_data_on_disk
					* c_disk 
					+ r1.m_bucket_metadata.m_bucket_data_in_memory
					* r1.m_bucket_metadata.m_bucket_data_in_memory
					* c_mem		
					+ r1.m_bucket_metadata.m_bucket_load
					* r1.m_bucket_metadata.m_bucket_load
					* c_load;
		double n2 = r2.m_bucket_metadata.m_bucket_data_on_disk
					* r2.m_bucket_metadata.m_bucket_data_on_disk
					* c_disk
					+ r2.m_bucket_metadata.m_bucket_data_in_memory
					* r2.m_bucket_metadata.m_bucket_data_in_memory
					* c_mem
					+ r2.m_bucket_metadata.m_bucket_load
					* r2.m_bucket_metadata.m_bucket_load
					* c_load;
		return n1 > n2;
	}
};

inline static double compute_transfer(
		WorkerSchedulingData* wsd,
		const double send,
		const double recv,
		const double read,
		const double write,
		bool use_disk){
	double net_out = (wsd->read + read + wsd->sent + send) 
						/ wsd->bandwidth_network;
	double net_in  = (wsd->written + write + wsd->recv + recv) 
						/ wsd->bandwidth_network;

	if (use_disk){
		double disk = (wsd->read + read) / wsd->bandwidth_disk_read 
						+ (wsd->written + write) / wsd->bandwidth_disk_write;
		return std::max(std::max(net_out,net_in),disk);
	}
	return std::max(net_out,net_in);
}

inline static double pow3(double a){
	return a*a*a;
}

ReturnCode Master::rebalance_providers(
            const std::string& service_name,
			const std::unordered_map<WorkerInfo, 
					int, WorkerInfoHash>& commissioned_providers,
            const std::unordered_map<WorkerInfo, 
					std::vector<ProviderInfo>, 
					WorkerInfoHash>& decommissioned_providers,
            const std::vector<WorkerInfo>& hosts,
			const std::unordered_map<WorkerInfo, 
				WorkerQuotas, 
				WorkerInfoHash>& workers,
			const ProviderRescalingParameters prp){
	ReturnCode rc;
	
	
	// Rebalancing metadata
	double WL = prp.weight_load;
	double WS = prp.weight_data;
	double WD = prp.weight_transfers;
	bool use_disk = prp.use_disk;
	bool use_mem  = prp.use_mem;
	int parallel_migrations = prp.parallel_migrations;

	// Basic structures containing all informations about providers and workers
	std::vector<WorkerSchedulingData> worker_data(hosts.size());
	std::vector<ProviderSchedulingData> provider_data;
	std::unordered_map<WorkerInfo, 
		WorkerSchedulingData*, WorkerInfoHash> worker_data_map;
	std::unordered_map<ProviderInfo, 
		ProviderSchedulingData*, ProviderInfoHash> provider_data_map;

	// Fill in worker_data
    std::unordered_map<WorkerInfo,
		WorkerQuotas,WorkerInfoHash>::const_iterator cwqit;
	int index = 0;
	for (cwqit = workers.cbegin(); cwqit != workers.cend(); ++ cwqit){
		
		WorkerSchedulingData& wsd = worker_data[index];
		wsd.bandwidth_network 	= cwqit->second.m_bandwidth_network;
		wsd.bandwidth_disk_read = cwqit->second.m_bandwidth_disk_read;
		wsd.bandwidth_disk_write= cwqit->second.m_bandwidth_disk_write;
		wsd.quota_disk 			= cwqit->second.m_quota_disk;
		wsd.quota_memory		= cwqit->second.m_quota_memory;
		wsd.wi 					= cwqit->first;
		
		worker_data_map[cwqit->first] = worker_data.data() + index;
		index++;
	}
	
	// Initiate rescaling
	std::vector<tl::async_response> requests;

	int nb_commissioned_providers = 0;

	m_workers_mtx->lock();
	for (const WorkerInfo& wi : hosts){	
		int nb_to_com = 0;
		if (commissioned_providers.count(wi) > 0){
			nb_to_com = commissioned_providers.at(wi);
			worker_data_map[wi]->before_hosted_providers -= nb_to_com;
		}
		if (decommissioned_providers.count(wi) > 0){
			requests.push_back(
				m_initiate_rescaling
					.on(*(m_workers[wi].m_handle))
					.async(service_name, 
							nb_to_com, 
							decommissioned_providers.at(wi)));
		} else {
			std::vector<ProviderInfo> empty;
			requests.push_back(
				m_initiate_rescaling
					.on(*(m_workers[wi].m_handle))
					.async(service_name, 
							nb_to_com, 
							empty));
		}
		nb_commissioned_providers += nb_to_com;
	}

	

	for (tl::async_response& resp : requests){
		ReturnCode ret = resp.wait().as<ReturnCode>();
		if (!ret){
			ret.interpret();
			if (rc){
				rc = ret;
			}
		}
	}
	m_workers_mtx->unlock();
	requests.clear();

	

	if (rc){


		int nb_buckets = 0;

		// Collect buckets
		std::unordered_map<ProviderInfo,
			ProviderSnapshot, ProviderInfoHash> provider_buckets;

		auto before = std::chrono::system_clock::now();

		// get all buckets for a service_name on host
		provider_buckets = get_all_provider_buckets(service_name, WL,WS, hosts);

		// Fill provider_data
		provider_data.resize(provider_buckets.size());
		index = 0;

		std::unordered_map<ProviderInfo,
			ProviderSnapshot, 
			ProviderInfoHash>::const_iterator cppit;
		for (cppit = provider_buckets.cbegin(); 
				cppit != provider_buckets.cend(); 
				++cppit){

			const WorkerInfo& host = cppit->second.m_worker_info;

			ProviderSchedulingData& psd = provider_data[index];
			psd.pi 		= cppit->first;

			assert(worker_data_map.count(host) > 0);

			psd.host	= worker_data_map[host];
			psd.total_data_mem = cppit->second.m_provider_metadata.m_data_in_memory;
			psd.total_data_disk = cppit->second.m_provider_metadata.m_data_on_disk;
			psd.total_load = cppit->second.m_provider_metadata.m_total_load;
			psd.max_load = cppit->second.m_provider_metadata.m_max_load;
			psd.max_data_mem = cppit->second.m_provider_metadata.m_max_data_mem;
			psd.max_data_disk = cppit->second.m_provider_metadata.m_max_data_disk;

			provider_data_map[cppit->first] = provider_data.data() + index;
			index++;

			nb_buckets += cppit->second.m_buckets.size();
		}

		// Mark which provider is decommissioned
		

		int nb_decommissioned_providers = 0;

		std::unordered_map<WorkerInfo, 
			std::vector<ProviderInfo>, 
			WorkerInfoHash>::const_iterator cwvit;
		for (cwvit = decommissioned_providers.cbegin(); 
				cwvit != decommissioned_providers.cend(); 
				++ cwvit){
			for (const ProviderInfo& pi : cwvit->second){
				// No need to check for validity
				provider_data_map[pi]->in_decommission=true;
			}
			nb_decommissioned_providers += cwvit->second.size();
		}

		// XXX Reorder not in decommission first then the ones in decommission


		// Compute targets
		

		// Compute number of hosts atthe end of the rescaling
		int nb_nodes_after_rescaling = provider_buckets.size() 
			- nb_decommissioned_providers;
		int nb_nodes_before_rescaling = provider_buckets.size()
			- nb_commissioned_providers;

		// need
		double total_load 		= 0;
		double total_data_disk 	= 0;
		double total_data_mem	= 0;

		// Per bucket
		double max_load 		= 0;
		double max_data_disk 	= 0;
		double max_data_mem 	= 0;

		for (const ProviderSchedulingData& psd : provider_data){
			// If the provider is not being decommissioned increment 
			// number of providers hosted 
			if (!psd.in_decommission)
				psd.host->final_hosted_providers++;
			psd.host->before_hosted_providers++;

			total_load 		+= psd.total_load;
			total_data_disk += psd.total_data_disk;
			total_data_mem 	+= psd.total_data_mem;

			max_load 	  = std::max(max_load,psd.max_load);
			max_data_disk = std::max(max_data_disk,psd.max_data_disk);
			max_data_mem  = std::max(max_data_mem,psd.max_data_mem);
		}

		
		// Compute the total disk read bandwidth and network bandwidth on the 
		// final sized cluster.
		// The idea is that, in case of a worker decommission, it last for the same
		// duration, independently of the choice of the worker.
		double total_disk_read_bw = 0;
		double total_network_bw = 0;

		for (const WorkerSchedulingData& wsd : worker_data){
			if (wsd.final_hosted_providers > 0){
				total_disk_read_bw 	+= wsd.bandwidth_disk_read;
				total_network_bw 	+= wsd.bandwidth_network;
			}
		}

		assert(total_network_bw > 0);
		assert(!use_disk || total_disk_read_bw > 0);

		
		// Target load for the providers
		double target_load = total_load / nb_nodes_after_rescaling;
		if (max_load > target_load)
			target_load = max_load;
		
		double current_ideal_load = total_load / nb_nodes_before_rescaling;
		if (max_load > current_ideal_load)
			current_ideal_load = max_load;

		// Target mem and disk for each provider
		for (ProviderSchedulingData& psd : provider_data){
			if (psd.in_decommission)
				continue;
			if (use_disk){
				psd.target_disk = std::min(
						total_data_disk
						* psd.host->bandwidth_disk_read
						/ psd.host->final_hosted_providers
						/ total_disk_read_bw,
						psd.host->quota_disk
						/ psd.host->final_hosted_providers);
				psd.previous_ideal_disk = std::min(
						total_data_disk
						* psd.host->bandwidth_disk_read
						/ psd.host->before_hosted_providers
						/ total_disk_read_bw,
						psd.host->quota_disk
						/ psd.host->before_hosted_providers);	
			} 
			if (use_mem){
				psd.target_mem = std::min(
						(total_data_mem + total_data_disk)
						* psd.host->bandwidth_network
						/ psd.host->final_hosted_providers
						/ total_network_bw
						- psd.target_disk,
						psd.host->quota_memory
						/ psd.host->final_hosted_providers);
				psd.previous_ideal_mem = std::min(
						(total_data_mem + total_data_disk)
						* psd.host->bandwidth_network
						/ psd.host->before_hosted_providers
						/ total_network_bw
						- psd.previous_ideal_disk,
						psd.host->quota_memory
						/ psd.host->before_hosted_providers);
			}
			psd.target_load = target_load;
			psd.previous_ideal_load = current_ideal_load;
		}


		
		// Target transfers per provider 
		double target_transfers = 0;
		for (const ProviderSchedulingData& psd : provider_data){
			double transfer_solo = 0;
			if (psd.in_decommission){
				transfer_solo = (psd.total_data_mem + psd.total_data_disk)
					/psd.host->bandwidth_network;
				if (use_disk){
					double tmp = psd.total_data_disk / psd.host->bandwidth_disk_read;
					transfer_solo = std::max(transfer_solo, tmp);
				}
			} else {
				double tmp_disk = 0;

				double data_recv = 0;
				double data_send = 0;

				if (use_disk){
					// Data received by the provider
					double delta_disk = psd.target_disk - psd.total_data_disk;
					if (delta_disk > 0){
						data_recv += delta_disk;
						tmp_disk = delta_disk / psd.host->bandwidth_disk_write;
					} else {
						data_send -= delta_disk;
						tmp_disk = -delta_disk / psd.host->bandwidth_disk_read;
					}
				}

				double delta_mem = psd.target_mem - psd.total_data_mem;
				if (delta_mem > 0){
					data_recv += delta_mem;
				} else {
					data_send -= delta_mem;
				}

				double tmp_mem = std::max(data_send,data_recv)
					/psd.host->bandwidth_network;
				transfer_solo = std::max(tmp_disk,tmp_mem);
			}
			target_transfers = std::max(target_transfers, transfer_solo);
		}

		// Adjust targets
		for (ProviderSchedulingData& psd : provider_data){
			if (WD < 1){
				psd.target_load = WD * psd.target_load 
					+ (1-WD) * psd.previous_ideal_load;
				psd.target_disk = WD * psd.target_disk 
					+ (1-WD) * psd.previous_ideal_disk;
				psd.target_mem = WD * psd.target_mem 
					+ (1-WD) * psd.previous_ideal_mem;	
			}
			psd.target_load *= WL;
			psd.target_mem 	*= WS;
			psd.target_disk	*= WS;
		}
		target_transfers *= WD;

		
		// Preallocate
		std::vector<BucketInfo> unallocated_buckets;
		for (cppit = provider_buckets.cbegin(); 
				cppit != provider_buckets.cend(); 
				++cppit){

			ProviderSchedulingData* psd = provider_data_map[cppit->first];

			bool preallocation_done = psd->in_decommission;

			for (const BucketInfo& ri : cppit->second.m_buckets){
				if (preallocation_done){
					unallocated_buckets.push_back(ri);
				} else {
					if (psd->load + ri.m_bucket_metadata.m_bucket_load 
							<= psd->target_load 
							&& psd->mem + ri.m_bucket_metadata
									.m_bucket_data_in_memory
								<= psd->target_mem
							&& (!use_disk 
								|| psd->disk + ri.m_bucket_metadata
										.m_bucket_data_on_disk
									<= psd->target_disk)){
						psd->mem += ri.m_bucket_metadata.m_bucket_data_in_memory;
						psd->disk += ri.m_bucket_metadata.m_bucket_data_on_disk;
						psd->load += ri.m_bucket_metadata.m_bucket_load;
					} else {
						preallocation_done = true;
						unallocated_buckets.push_back(ri);
					}
				} 
			}

			psd->host->mem += psd->mem;
			psd->host->disk += psd->disk;	
		}
		
		// Schedule transfers
		std::unordered_map<WorkerInfo,MigrationPlan,WorkerInfoHash> migrationPlans;

		// Sort data per size
		std::sort(unallocated_buckets.begin(), 
				unallocated_buckets.end(), 
				CompareBucketInfo(WL,WS,
					total_data_disk / nb_buckets, 
					total_data_mem / nb_buckets, 
					total_load / nb_buckets));

		// Compute current penalties
		for (ProviderSchedulingData& psd : provider_data){
			if (psd.in_decommission){
				psd.penalty = std::numeric_limits<double>::max(); 
				continue;
			}
			double penalty = 0;
			if (use_mem){
				penalty += pow3(psd.mem/psd.target_mem);
			}
			// Penalty for disk
			if (use_disk){
				penalty += pow3(psd.disk / psd.target_disk);
				if (use_mem)
					penalty /= 2;
			}
			// Penalty for load
			penalty += pow3( psd.load / psd.target_load);

			psd.penalty = penalty;
		}


		int iterations = 0;


		// For all remaining data 
		for (const BucketInfo& ri : unallocated_buckets){

			double load = ri.m_bucket_metadata.m_bucket_load;
			double disk = ri.m_bucket_metadata.m_bucket_data_on_disk;
			double mem  = ri.m_bucket_metadata.m_bucket_data_in_memory;


			ProviderSchedulingData* src = provider_data_map[ri.m_provider_info];

			// Initialize best_canadidate as no move
			ProviderInfo best_pi = src->pi;
			double best_penalty = 0;
			int checked_dests = 0;

			if (src->in_decommission){
				best_penalty = std::numeric_limits<double>::max();
			} else {
				checked_dests++;
				// Penalty for mem
				if (use_mem){
					best_penalty += pow3((src->mem + mem)/ src->target_mem);
				}
				// Penalty for disk
				if (use_disk){
					best_penalty += pow3((src->disk + disk)/ src->target_disk);
					if (use_mem)
						best_penalty /= 2;
				}
				// Penalty for load
				best_penalty += pow3((src->load + load)/ src->target_load);
				// Transfers penalty
				best_penalty += pow3(
						compute_transfer(src->host,0,0,0,0,use_disk)
						/target_transfers);
			}
			// Compute penalty part of src (moving data out of src)
			double src_penalty = 0.5*pow3(
					compute_transfer(src->host,disk+mem,0,disk,0,use_disk)
					/target_transfers);

			for (ProviderSchedulingData& psd : provider_data){
				iterations++;

				if (psd.in_decommission){
					continue;
				}

				if (psd.penalty >= best_penalty){
					continue;
				}

				// Check disk capacity
				if (use_disk && psd.host->disk + disk > psd.host->quota_disk){
					continue;
				}

				// Check memory capacity
				if (use_mem && psd.host->mem + mem > psd.host->quota_memory){
					continue;
				}

				// REPL Check replication
				checked_dests++;

				// Compute penalty	
				double penalty = 0;

				// Penalty for mem
				if (use_mem){
					penalty += pow3((psd.mem + mem)/ psd.target_mem);
				}
				// Penalty for disk
				if (use_disk){
					penalty += pow3((psd.disk + disk)/ psd.target_disk);
					if (use_mem)
						penalty /= 2;
				}
				// Penalty for load
				penalty += pow3((psd.load + load)/ psd.target_load);


				if (!(psd.pi == src->pi)){
					penalty += 0.5*pow3(
							compute_transfer(psd.host,0,disk+mem,0,disk,use_disk)
							/target_transfers);
					penalty += src_penalty;
				} else {
					penalty += pow3(
							compute_transfer(psd.host,0,0,0,0,use_disk)
							/target_transfers);
				}


				if (penalty < best_penalty){
					best_penalty = penalty;
					best_pi = psd.pi;
				}
			}

			if (checked_dests == 0){
				rc.m_error_code = NO_AVAILABLE_PROVIDER;
				rc.m_bucket = ri.m_bucket_tag;
				rc.m_worker = src->host->wi;
				rc.interpret();
				break;
			}

			ProviderSchedulingData* psd = provider_data_map[best_pi];

			// Update data
			psd->load += load;
			psd->disk += disk;
			psd->mem  += mem;

			if (psd->in_decommission){
				std::cerr << "Failure" << std::endl;
				// TODO 
				assert(false);
			}

			if (!(best_pi == src->pi)){
				psd->penalty = best_penalty - src_penalty;
				src->penalty += src_penalty;

				src->host->sent += mem + disk;
				src->host->read += disk;

				psd->host->recv += mem + disk;
				psd->host->written += disk;

				// Generate migration operation
				MigrationOperation op;
				op.m_bucket 		= ri.m_bucket_tag;
				op.m_source			= src->pi;
				op.m_destination 	= best_pi;

				migrationPlans[src->host->wi].m_operations.push_back(op);
			} else {
				psd->penalty = best_penalty;
			}
		}

		auto after = std::chrono::system_clock::now();
		std::chrono::duration<double> diff = after-before;
		m_duration_last_scheduling = diff.count();
		// Update data if stalled
		// std::cout << diff.count() << "s scheduling" << std::endl;

		/*
		   for (ProviderSchedulingData& psd : dests){
		   std::cout << psd.penalty << " : "; 
		   std::cout << psd.mem << "/" << psd.target_mem << " ";
		   std::cout << psd.disk << "/" << psd.target_disk << " ";
		   std::cout << psd.load << "/" << target_load << " ";
		   std::cout << target_transfers;
		   std::cout << std::endl;
		   }
		//*/

		// Migrate data

		if (rc){
			//std::cout << "Master: sending orders" << std::endl;
			int parallel_mig = std::min(parallel_migrations,
									(int)std::ceil(1.0*parallel_migrations
									*nb_nodes_after_rescaling/nb_nodes_before_rescaling)); 
			rc = send_migration_plan(migrationPlans,parallel_mig);
		}
	}


	// Terminate rescaling
	m_workers_mtx->lock();

	
	for (const WorkerInfo& wi : hosts){	
		requests.push_back(
			m_terminate_rescaling
				.on(*(m_workers[wi].m_handle))
				.async(service_name,!rc));
	}

	for (tl::async_response& resp : requests){
		ReturnCode ret = resp.wait().as<ReturnCode>();
		if (!ret){
			ret.interpret();
			if (rc){
				rc = ret;
			}
		}
	}
	m_workers_mtx->unlock();
	requests.clear();

	//std::cout << "Master: terminating" << std::endl;
	

	// Return return code
	return rc;
}




ReturnCode Master::on_commission(
		const std::unordered_map<WorkerInfo, std::vector<std::string>, 
			WorkerInfoHash>& workers){


	ReturnCode rc;
	// Acquire operation mutex
	if (!m_op_mtx->try_lock()){
		rc.m_error_code = OPERATION_RUNNING;
		return rc;
	}


	std::unordered_map<WorkerInfo, 
		std::vector<std::string>, WorkerInfoHash>::const_iterator cit;
	m_workers_mtx->lock();
	
	
	for (cit = workers.cbegin(); cit != workers.cend(); ++cit){
		if (m_workers.count(cit->first) == 0){
			m_workers_mtx->unlock();
			rc.m_error_code = error_codes::UNKNOWN_WORKER; 
			rc.m_worker = cit->first;
			rc.interpret();
			m_op_mtx->unlock();
			return rc;
		}

		const WorkerMetadata& wm = m_workers[cit->first].m_worker_metadata;

		// Check worker metadata: Need for disk
		for (const std::string& service_name : cit->second){
			if (provider_parameters.count(service_name)>0){
				if (provider_parameters[service_name].use_disk){
					if (!wm.m_has_disk){
						// Provider needs disk but worker hasn't got one
						m_workers_mtx->unlock();
						rc.m_error_code = error_codes::NO_DISK; 
						rc.m_worker = cit->first;
						rc.m_service_name = service_name;
						rc.interpret();
						m_op_mtx->unlock();
						return rc;
					}
				}
			}
		}	
	}

	// Start providers on workers
    std::unordered_map<WorkerInfo, 
		std::unordered_map<std::string,int>, 
		WorkerInfoHash> commissioned_providers;
	for (cit = workers.cbegin(); cit != workers.cend(); ++cit){
		for (const std::string& service_name : cit->second){
			if (commissioned_providers[cit->first].count(service_name) == 0){
				commissioned_providers[cit->first][service_name] = 1;
			} else {
				commissioned_providers[cit->first][service_name]++;
			}
		}
	}

	m_workers_mtx->unlock();

	// Call rebalancing
    std::unordered_map<WorkerInfo, 
		std::vector<ProviderInfo>, WorkerInfoHash> decommissioned_providers;

	rc = rebalancing(commissioned_providers,decommissioned_providers,false);

	m_op_mtx->unlock();
	return rc;
}

ReturnCode Master::on_provider_decommission(
            const std::unordered_map<WorkerInfo, 
					std::vector<ProviderInfo>, 
					WorkerInfoHash>& decommissioned_providers){
	
	

	ReturnCode rc;
	// Acquire operation mutex
	if (!m_op_mtx->try_lock()){
		rc.m_error_code = OPERATION_RUNNING;
		rc.interpret();
		return rc;
	}
	
	// Call rebalancing
    std::unordered_map<WorkerInfo, 
		std::unordered_map<std::string,int>, 
		WorkerInfoHash> commissioned_providers;
	
	// Check that the workers exist
    std::unordered_map<WorkerInfo, std::vector<ProviderInfo>, 
		WorkerInfoHash>::const_iterator cit;
	m_workers_mtx->lock();
	for (cit = decommissioned_providers.begin(); 
			cit != decommissioned_providers.end(); 
			++cit){
		if (m_workers.count(cit->first) == 0){
			m_workers_mtx->unlock();
			rc.m_error_code = error_codes::UNKNOWN_WORKER; 
			rc.m_worker = cit->first;
			rc.interpret();
			m_op_mtx->unlock();
			return rc;
		} 
	}
	m_workers_mtx->unlock();
	
	rc = rebalancing(commissioned_providers,decommissioned_providers,false);
	
	m_op_mtx->unlock();
	return rc;
}

ReturnCode Master::on_decommission(const std::vector<WorkerInfo>& workers){
	ReturnCode rc;
	
	
	
	// Acquire operation mutex
	if (!m_op_mtx->try_lock()){
		rc.m_error_code = OPERATION_RUNNING;
		rc.interpret();
		return rc;
	}
	
    std::unordered_map<WorkerInfo, 
		std::unordered_map<std::string,int>, 
		WorkerInfoHash> commissioned_providers;
    std::unordered_map<WorkerInfo, 
		std::vector<ProviderInfo>, 
		WorkerInfoHash> decommissioned_providers;

	m_workers_mtx->lock();
	for (const WorkerInfo& wi : workers){
		// Check that the workers exist
		if (m_workers.count(wi) == 0){
			m_workers_mtx->unlock();
			rc.m_error_code = error_codes::UNKNOWN_WORKER; 
			rc.m_worker = wi;
			m_op_mtx->unlock();
			rc.interpret();
			return rc;
		} 
		decommissioned_providers[wi];
	}
	m_workers_mtx->unlock();

	rc = rebalancing(commissioned_providers,decommissioned_providers,true);
	
	m_op_mtx->unlock();
	return rc;
}

ReturnCode Master::on_leave(const WorkerInfo& workerInfo){
	ReturnCode rc;
	// Refuse the worker to leave if there is an operation running
	if (!m_op_mtx->try_lock()){
		rc.m_error_code = OPERATION_RUNNING;
		return rc;
	}
	m_workers_mtx->lock();
	if (m_workers.count(workerInfo) == 0){
		m_workers_mtx->unlock();
		rc.m_error_code = error_codes::UNKNOWN_WORKER; 
		rc.m_worker = workerInfo;
		m_op_mtx->unlock();
		rc.interpret();
		return rc;
	} 
	m_workers.erase(workerInfo);	
	m_workers_mtx->unlock();
	m_op_mtx->unlock();
	return rc;
}


bool Master::configure_provider(
		const std::string& service_name,
		double weight_load,
		double weight_data,
		double weight_transfers,
		bool use_disk,
		bool use_memory,
		int parallel_migrations){

	if (!use_disk && !use_memory){
		return false;
	}
	
	if (weight_load <= 0 || weight_data <= 0 || weight_transfers <= 0){
		return false;
	}
	
	double wl = 1/weight_load;
	double wd = 1/weight_data;
	double wt = 1/weight_transfers;

	ProviderRescalingParameters& prp = provider_parameters[service_name];
	
	double norm = std::min(wl,wd);

	prp.weight_load = wl / norm;
	prp.weight_data = wd / norm;
	prp.weight_transfers = wt / norm;

	prp.use_disk = use_disk;
	prp.use_mem = use_memory;

	prp.parallel_migrations = parallel_migrations;

	return true;
}

double Master::get_duration_scheduling(){
	return m_duration_last_scheduling;
}
}

