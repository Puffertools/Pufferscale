/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "pufferscale/Controller.hpp"

#include <cassert>

#include "pufferscale/PufferscaleExceptions.hpp"
#include "pufferscale/ReturnCode.hpp"

#define MARK std::cout << __FILE__<< ":" << __LINE__ << std::endl;

namespace pufferscale {

Controller::Controller(
        tl::engine& e,
        const MasterInfo& master)
: m_engine(e)
, m_commission(            	e.define("pufferscale_commission"))
, m_decommission(          	e.define("pufferscale_decommission"))
, m_decommission_provider( 	e.define("pufferscale_provider_decommission"))
, m_list_all_buckets(    	e.define("pufferscale_list_all_buckets_master"))
, m_list_all_providers(    	e.define("pufferscale_list_all_providers_master"))
{

	tl::endpoint masterEndpoint = e.lookup(master.m_address);
    m_master = std::unique_ptr<tl::provider_handle>(
		new tl::provider_handle(masterEndpoint, master.m_provider_id));
}

Controller::~Controller() {}

bool Controller::commission(
        const WorkerInfo& workerInfo,
        const std::vector<std::string>& providerInfo) const {
	std::unordered_map<WorkerInfo, std::vector<std::string>, WorkerInfoHash> map;
	map[workerInfo] = providerInfo;
   	return commission(map); 
}

bool Controller::commission(
        const std::unordered_map<WorkerInfo, 
			std::vector<std::string>, 
			WorkerInfoHash>& workers) const {
	ReturnCode rc = m_commission.on(*m_master)(workers);
	return interpret_return_code(rc);
}

bool Controller::decommission(
        const WorkerInfo& workerInfo,
        const std::vector<ProviderInfo>& providerInfo) const {
    std::unordered_map<WorkerInfo, 
		std::vector<ProviderInfo>, 
		WorkerInfoHash> map;
	map[workerInfo] = providerInfo;
    return decommission(map);
}

bool Controller::decommission(
        const std::unordered_map<WorkerInfo, 
			std::vector<ProviderInfo>, 
			WorkerInfoHash>& workers) const {
    ReturnCode rc = m_decommission_provider.on(*m_master)(workers);
	return interpret_return_code(rc);
}

bool Controller::decommission(
        const WorkerInfo& workerInfo) const {
    std::vector<WorkerInfo> vect;
	vect.push_back(workerInfo);
	return decommission(vect);
}

bool Controller::decommission(
        const std::vector<WorkerInfo>& workers) const {
    ReturnCode rc = m_decommission.on(*m_master)(workers);
	return interpret_return_code(rc);
}

std::unordered_map<WorkerInfo,
			std::vector<ProviderInfo>,
			WorkerInfoHash>  Controller::list_all_providers() const {
    
	size_t buff_size = 1024;
	int request_id = 0;

	tl::buffer buff(buff_size);
	std::vector<std::pair<void*,std::size_t>> segments(1);
	segments[0].first  = (void*)(buff.data());
	segments[0].second = buff_size;
	
	tl::bulk myBulk = m_engine.expose(segments, tl::bulk_mode::read_write);
	
	std::pair<ReturnCode,std::pair<size_t,int>> status;
	status = m_list_all_providers
		.on(*m_master)(buff_size,myBulk,request_id)
		.as<std::pair<ReturnCode,std::pair<size_t,int>>>();
	
	while (status.first.m_error_code == BUFFER_SIZE){
		buff_size = status.second.first;
		request_id = status.second.second;

		buff.resize(buff_size);
	
		segments[0].first  = (void*)(buff.data());
		segments[0].second = buff_size;
		myBulk = m_engine.expose(segments, tl::bulk_mode::read_write);
		
		status = m_list_all_providers
			.on(*m_master)(buff_size,myBulk,request_id)							
			.as<std::pair<ReturnCode,std::pair<size_t,int>>>();
	}

	interpret_return_code(status.first);
	assert(status.first);

	tl::buffer_input_archive boa(buff,m_engine);
	std::unordered_map<WorkerInfo,
			std::vector<ProviderInfo>,
			WorkerInfoHash> results;

	boa & results;

	return results;
}

std::unordered_map<WorkerInfo,
	std::vector<BucketInfo>,
	WorkerInfoHash> Controller::list_all_buckets() const {
    
	size_t buff_size = 1024;
	int request_id = 0;
	
	tl::buffer buff(buff_size);
	std::vector<std::pair<void*,std::size_t>> segments(1);
	segments[0].first  = (void*)(buff.data());
	segments[0].second = buff_size;
	
	tl::bulk myBulk = m_engine.expose(segments, tl::bulk_mode::read_write);
	

	std::pair<ReturnCode,std::pair<size_t,int>> status;
	status = m_list_all_buckets
		.on(*m_master)(buff_size,myBulk,request_id)
		.as<std::pair<ReturnCode,std::pair<size_t,int>>>();

	while (status.first.m_error_code == BUFFER_SIZE){
		buff_size = status.second.first;
		request_id = status.second.second;

		buff.resize(buff_size);

		segments[0].first  = (void*)(buff.data());
		segments[0].second = buff_size;
		myBulk = m_engine.expose(segments, tl::bulk_mode::read_write);
		
		status = m_list_all_buckets
			.on(*m_master)(buff_size,myBulk,request_id)
			.as<std::pair<ReturnCode,std::pair<size_t,int>>>();
	}
	interpret_return_code(status.first);

	assert(status.first);

	tl::buffer_input_archive boa(buff,m_engine);

	std::unordered_map<WorkerInfo,std::vector<BucketInfo>,WorkerInfoHash> results;
	boa & results;
	
	return results;
}

bool Controller::interpret_return_code(const ReturnCode& rc) const{
	if (rc.m_multiple_errors){
		std::cout << "Multiple errors occured, first one follows: " << std::endl;
	}
	
	if (rc.m_error_code == error_codes::ALL_GOOD)
		return true;

	if (rc.m_error_code == error_codes::UNKNOWN_WORKER)	
		throw UnknownWorkerException(rc.m_worker);
	
	else if (rc.m_error_code == error_codes::ALREADY_KNOWN_WORKER)
		throw AlreadyKnownWorkerException(rc.m_worker); 
	
	else if (rc.m_error_code == error_codes::PROVIDER_PRESENT)	
		throw ProviderAlreadyPresentException(rc.m_worker,rc.m_provider);
	
	else if (rc.m_error_code == error_codes::PROVIDER_CONSTRAINTS)
		throw ProviderConstraintsException(rc.m_worker,rc.m_provider);
	
	else if (rc.m_error_code == error_codes::UNKNOWN_PROVIDER)
		throw ProviderNotPresentException(rc.m_worker,rc.m_provider); 
	
	else if (rc.m_error_code == error_codes::NO_AVAILABLE_PROVIDER)
		throw NoProviderToMoveBucketException(rc.m_bucket, rc.m_worker);
	
	else if (rc.m_error_code == error_codes::UNKNOWN_BUCKET)
		throw UnknownBucketException(rc.m_worker, rc.m_bucket);
	
	else if (rc.m_error_code == error_codes::BUCKET_NOT_MIGRATED)
		throw BucketNotMigratedException(rc.m_worker, rc.m_bucket);
	
	else if (rc.m_error_code == error_codes::BUCKETS_ON_PROVIDER)
		throw ProviderCannotBeDecommissionedException(rc.m_worker, rc.m_provider);	
	
	else if (rc.m_error_code == error_codes::PROVIDER_TYPE_UNKNOWN)
		throw ProviderTypeUnknownException(rc.m_worker,rc.m_service_name);	
	
	else if (rc.m_error_code == error_codes::TOO_MUCH_DATA)
		throw TooMuchDataException(rc.m_service_name);	
	
	else if (rc.m_error_code == error_codes::PROVIDER_NOT_COMMISSIONED)
		throw ProviderNotCommissionedException(rc.m_worker,rc.m_service_name);	
	
	else std::cout << "Error code " << rc.m_error_code << std::endl;

	assert(false);
	return false;
}

}
