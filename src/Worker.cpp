/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "pufferscale/Worker.hpp"

#include <sys/types.h>
#include <time.h>

#include <iostream>
#include <algorithm>
#include <cassert>
#include <condition_variable>

#include "pufferscale/PufferscaleExceptions.hpp"
#include "pufferscale/Internals.hpp"

#define MARK std::cout << __FILE__<< ":" << __LINE__ << std::endl;

namespace pufferscale {

Worker::Worker(tl::engine& e, uint16_t provider_id)
: tl::provider<Worker>(e, provider_id)
, m_join(e.define("pufferscale_join")) 
, m_leave(e.define("pufferscale_leave")) {
    /* RPC that a Worker can receive */
    define("pufferscale_migrate", &Worker::on_migrate);
    define("pufferscale_list_all_buckets", &Worker::on_list_all_buckets);
    define("pufferscale_list_provider_buckets", &Worker::on_list_provider_buckets);
    define("pufferscale_list_providers", &Worker::on_list_providers);
    define("pufferscale_initiate_rescaling", &Worker::on_initiate_rescaling);
    define("pufferscale_terminate_rescaling", &Worker::on_terminate_rescaling);
	/* Set on_finalize callback */
    e.push_finalize_callback([this]() { this->on_finalize(); });
    /* Set workerInfo */
	m_this_workerInfo.m_address = std::string(get_engine().self());
    m_this_workerInfo.m_provider_id = get_provider_id();

	m_buckets_mtx = std::unique_ptr<tl::mutex>(new tl::mutex);
	
	e.enable_remote_shutdown();
}

Worker::~Worker() {}

void Worker::on_finalize() {
	// Clean-up
	m_buckets_mtx->lock();
	m_managed_buckets.clear();
	m_buckets_mtx->unlock();
}

bool Worker::manages_provider(const ProviderInfo& provider){
	if(m_has_joined)
		return false;

	m_buckets_mtx->lock();
	
	if (m_managed_buckets.count(provider) > 0){
		m_buckets_mtx->unlock();
		throw ProviderAlreadyPresentException(m_this_workerInfo,provider);
	}

	if (m_provider_management.count(provider.m_service_name) == 0){
		m_buckets_mtx->unlock();
		throw ProviderTypeUnknownException(m_this_workerInfo,provider.m_service_name);
	}

	m_managed_buckets[provider].m_provider_metadata.m_provider_info = provider;
	m_need_metadata_update = true;
	m_buckets_mtx->unlock();

	return true;
}

bool Worker::manages(
		const ProviderInfo& provider,
		const BucketTag& bucket_tag,
        const MigrationCallback& migrationCallback,
		const MetadataUpdateCallback& metadataUpdateCallback,
		void* uargs) {

	if (m_provider_management.count(provider.m_service_name) == 0){
		throw ProviderTypeUnknownException(m_this_workerInfo,provider.m_service_name);
	}

	m_buckets_mtx->lock();

	// Check that provider exist
	if (m_managed_buckets.count(provider) == 0){
		m_buckets_mtx->unlock();
		throw ProviderNotPresentException(m_this_workerInfo,provider);
	}

	// Check that the provider is not in decommission
	if (m_managed_buckets[provider].m_in_decommission){
		m_buckets_mtx->unlock();
		return false;
	}

	// Check that the provider does not already host the bucket
	if (m_managed_buckets[provider].m_buckets.count(bucket_tag) > 0){
		m_buckets_mtx->unlock();
		throw BucketAlreadyPresentException(m_this_workerInfo, bucket_tag);
	}

	m_managed_buckets[provider].m_buckets[bucket_tag].m_bucket_info.m_bucket_tag = bucket_tag;
	m_managed_buckets[provider].m_buckets[bucket_tag].m_bucket_info.m_provider_info = provider;
	m_managed_buckets[provider].m_buckets[bucket_tag].m_migrationCallback = migrationCallback;
	m_managed_buckets[provider].m_buckets[bucket_tag].m_metadataUpdateCallback = metadataUpdateCallback;
	m_managed_buckets[provider].m_buckets[bucket_tag].m_uargs = uargs;

	m_need_metadata_update = true;

	m_buckets_mtx->unlock();

	return true;
}

bool Worker::stop_managing(
		const ProviderInfo& provider,
        const BucketTag& bucket_tag){
	
	if (m_provider_management.count(provider.m_service_name) == 0){
		throw ProviderTypeUnknownException(m_this_workerInfo,provider.m_service_name);
	}
	
	m_buckets_mtx->lock();

	// Check that provider exist
	if (m_managed_buckets.count(provider) == 0){
		m_buckets_mtx->unlock();
		throw ProviderNotPresentException(m_this_workerInfo,provider);
	}

	// Check that the provider is not in decommission
	if (m_managed_buckets[provider].m_in_rescaling){
		return false;
	}

	// Check that the provider hosts the bucket
	if (m_managed_buckets[provider].m_buckets.count(bucket_tag) == 0){
		m_buckets_mtx->unlock();
		throw BucketNotPresentException(m_this_workerInfo, bucket_tag);
	}

	m_managed_buckets[provider].m_buckets.erase(bucket_tag);
	m_need_metadata_update = true;
	
	m_buckets_mtx->unlock();

	return true;
}


bool Worker::configure_worker(double memory_capacity, double network_bandwidth){
	assert(!m_has_joined);
	if(m_has_joined)
		return false;
	if (memory_capacity <= 0)
		throw IncorrectMemoryCapacityException(m_this_workerInfo,memory_capacity);
	if (network_bandwidth <= 0)
		throw IncorrectNetworkBandwidthException(m_this_workerInfo,network_bandwidth);
	m_this_workerMetadata.m_bandwidth_network = network_bandwidth;
	m_this_workerMetadata.m_capacity_memory = memory_capacity;
	return true;
}

bool Worker::configure_disk(double capacity, double read_bw, double write_bw){
	assert(!m_has_joined);
	if(m_has_joined)
		return false;
	if (capacity <= 0)
		throw IncorrectDiskCapacityException(m_this_workerInfo,capacity);
	if (read_bw <= 0)
		throw IncorrectReadBandwidthException(m_this_workerInfo,read_bw);
	if (write_bw <= 0)
		throw IncorrectReadBandwidthException(m_this_workerInfo,write_bw);
	m_this_workerMetadata.m_has_disk = true;
	m_this_workerMetadata.m_bandwidth_disk_read = read_bw;
	m_this_workerMetadata.m_bandwidth_disk_write = write_bw;
	m_this_workerMetadata.m_capacity_disk = capacity;
	return true;
}

bool Worker::join(
        const MasterInfo& masterInfo) {
	assert(!m_has_joined);
	m_masterInfo = masterInfo;
    auto masterEndpoint = get_engine().lookup(m_masterInfo.m_address);
    auto masterProviderHandle = tl::provider_handle(masterEndpoint, m_masterInfo.m_provider_id);
	
	// Checking metadata
	assert(m_this_workerMetadata.m_bandwidth_network > 0);
	assert(m_this_workerMetadata.m_capacity_memory > 0);
	
	assert(!m_this_workerMetadata.m_has_disk || m_this_workerMetadata.m_bandwidth_disk_read > 0);
	assert(!m_this_workerMetadata.m_has_disk || m_this_workerMetadata.m_bandwidth_disk_write > 0);
	assert(!m_this_workerMetadata.m_has_disk || m_this_workerMetadata.m_capacity_disk > 0);

	ReturnCode rc = m_join.on(masterProviderHandle)(m_this_workerInfo, m_this_workerMetadata);
	if (rc.m_error_code == error_codes::ALL_GOOD){
		m_has_joined = true;
		return true;
	}
	if (rc.m_error_code == error_codes::ALREADY_KNOWN_WORKER){
		throw AlreadyKnownWorkerException(rc.m_worker);
	}
	return false;
}

bool Worker::leave(){
	if (!m_has_joined){
		return false;
	}

	m_buckets_mtx->lock();
	
	std::unordered_map<ProviderInfo, 
						ProviderData, 
						ProviderInfoHash>::iterator it; 
	for(it = m_managed_buckets.begin(); it != m_managed_buckets.end(); ++it){
		if (it->second.m_in_rescaling 
				|| it->second.m_buckets.size() > 0){
			m_buckets_mtx->unlock();
			return false;
		}
	}

    auto masterEndpoint = get_engine().lookup(m_masterInfo.m_address);
    auto masterProviderHandle = tl::provider_handle(masterEndpoint, m_masterInfo.m_provider_id);
	ReturnCode rc = m_leave.on(masterProviderHandle)(m_this_workerInfo);
	
	if (rc.m_error_code == error_codes::OPERATION_RUNNING){
		m_buckets_mtx->unlock();
		return false;
	}
	if (rc.m_error_code != error_codes::ALL_GOOD){
		rc.interpret();
		m_buckets_mtx->unlock();
		return false;
	}
	
	// Shutdown each and every provider
	for (it = m_managed_buckets.begin(); 
			it != m_managed_buckets.end(); 
			++it){
		std::string service_name = 
			it->second.m_provider_metadata
				.m_provider_info.m_service_name;
		// m_provider_management[service_name] exists 
		m_provider_management[service_name]
			.m_terminate(
				it->second.m_provider_metadata.m_provider_info,
				it->second.m_provider_uargs,
				m_provider_management[service_name].m_constructor_uargs);
	}

	m_buckets_mtx->unlock();
	return true;
}



ReturnCode Worker::on_migrate(
					const MigrationPlan& migrationPlan, 
					int max_concurrent_transfers) {

	//std::cout << get_provider_id() << " has to transfer " << migrationPlan.m_operations.size() << std::endl;
	ReturnCode rc;

	// Threads
	auto es = tl::xstream::self();
	std::vector<tl::pool> pools = es.get_main_pools();
	std::vector<tl::managed<tl::thread>> threads; 

	m_buckets_mtx->lock();

	for (const MigrationOperation& mop: migrationPlan.m_operations){
		if (m_managed_buckets.count(mop.m_source) == 0){
			m_buckets_mtx->unlock();
			rc.m_error_code = error_codes::UNKNOWN_PROVIDER;
			rc.m_worker = m_this_workerInfo;
			rc.m_provider = mop.m_source;
			return rc;
		}
		if (m_managed_buckets[mop.m_source]
				.m_buckets.count(mop.m_bucket) == 0){
			m_buckets_mtx->unlock();
			rc.m_error_code = error_codes::UNKNOWN_BUCKET;
			rc.m_worker = m_this_workerInfo;
			rc.m_bucket = mop.m_bucket;

			return rc;
		}
	}
	m_buckets_mtx->unlock();


	int concurrent = 0;
	
	tl::mutex mtx;
	std::shared_ptr<tl::mutex> concurrent_mtx(new tl::mutex());

	std::vector<int> finished_threads;
	//std::vector<int> thread_status(migrationPlan.m_operations.size());

	tl::condition_variable cv;
	std::unique_lock<tl::mutex> lck(mtx);
	
	// Shuffle migrationPlan.m_operation to improve load balancing
	std::vector<MigrationOperation> mops;
	mops = migrationPlan.m_operations;
	std::random_shuffle(mops.begin(), mops.end());

	for (const MigrationOperation& mop: mops){
		int pos_in_queue = threads.size();
		threads.push_back(pools[0].make_thread(
				[&rc,
				 mop,
				 &cv,
				 &concurrent,
				 &mtx,
				 &concurrent_mtx,
				 pos_in_queue,
				 &finished_threads,
				 //&thread_status,
				 this](){
				m_buckets_mtx->lock();
				BucketData rd = m_managed_buckets[mop.m_source]
						.m_buckets[mop.m_bucket];
				//concurrent_mtx->lock();
				//thread_status[pos_in_queue] = 1;
				//concurrent_mtx->unlock();
				m_buckets_mtx->unlock();
				if (rd.m_migrationCallback(mop,rd.m_uargs)){
					m_buckets_mtx->lock();
					m_managed_buckets[mop.m_source].m_buckets.erase(mop.m_bucket);
					//concurrent_mtx->lock();
					//thread_status[pos_in_queue] = 2;
					//concurrent_mtx->unlock();
					m_buckets_mtx->unlock();
				} else {
					std::cout << BucketNotMigratedException(m_this_workerInfo,mop.m_bucket).what() << std::endl;
					ReturnCode r;
					r.m_error_code = error_codes::BUCKET_NOT_MIGRATED;
					r.m_worker = m_this_workerInfo;
					r.m_bucket = mop.m_bucket;
					if (rc.m_error_code == error_codes::ALL_GOOD){
						rc = r;
					} else {
						rc.m_multiple_errors = true;
					}
				}
				std::unique_lock<tl::mutex> lck(mtx);
				concurrent_mtx->lock();
				concurrent--;
				finished_threads.push_back(pos_in_queue);
				//thread_status[pos_in_queue] = 3;
				concurrent_mtx->unlock();
				cv.notify_all();
				//concurrent_mtx->lock();
				//thread_status[pos_in_queue] = 4;
				//concurrent_mtx->unlock();
			}
			));
		concurrent_mtx->lock();
		concurrent++;
		int read_concurrent = concurrent;
		concurrent_mtx->unlock();
		while (read_concurrent >= max_concurrent_transfers){
			cv.wait(lck);
			std::vector<int> tmp;
			concurrent_mtx->lock();
			tmp.swap(finished_threads);
			read_concurrent = concurrent;
			concurrent_mtx->unlock();
			for (int i: tmp){
				threads[i]->join();
			}
		}
	//*/
	}
	//*
	concurrent_mtx->lock();
	int read_concurrent = concurrent;
	concurrent_mtx->unlock();


	/*
    struct timeval  tv;
	struct timespec ts;
	gettimeofday(&tv, NULL);
	ts.tv_sec = tv.tv_sec;
	ts.tv_nsec = tv.tv_usec * 1000;
	//*/

	while (read_concurrent > 0){
		/*
		ts.tv_nsec += 100*1000000;
		if (ts.tv_nsec > 1000000000){
			ts.tv_sec += 1;
			ts.tv_nsec -= 1000000000;
		}
		if (!cv.wait_until(lck,&ts)){
			std::cout << "Worker: " << read_concurrent << " remaining" << std::endl;
			std::cout << "Timed out" << std::endl;
			concurrent_mtx->lock();
			for (int i = 0; i < thread_status.size(); ++i){
				std::cout << "Thread " << i << ": " << thread_status[i] << std::endl;
			}
			concurrent_mtx->unlock();
			
		}//*/
		cv.wait(lck);
		concurrent_mtx->lock();
		read_concurrent = concurrent;
		concurrent_mtx->unlock();
	}
	concurrent_mtx->lock();
	for (int i: finished_threads){
		threads[i]->join();
	}
	concurrent_mtx->unlock();
	//*/

 	return rc;
}
//*/

void Worker::update_metadata(int request_id){
	// Avoid requesting metadata updates if it is just a matter of small
	// buffer
	if (!m_need_metadata_update 
			&&request_id == m_last_metadata_update)
		return ;

	m_buckets_mtx->lock();
	std::unordered_map<ProviderInfo,ProviderData,ProviderInfoHash>::iterator pit; 
	std::unordered_map<BucketTag,BucketData,BucketTagHash>::iterator rit;
	for (pit = m_managed_buckets.begin(); pit != m_managed_buckets.end(); ++pit){
		double data_on_disk = 0;
		double data_in_memory = 0;
		double total_load = 0;
		double max_load = 0;
		double max_disk = 0;
		double max_mem = 0;
		for (rit = pit->second.m_buckets.begin(); rit != pit->second.m_buckets.end(); ++rit){
			// Update metadata	
			const BucketMetadata& rm = rit->second.m_metadataUpdateCallback(rit->first,rit->second.m_uargs);
			rit->second.m_bucket_info.m_bucket_metadata = rm;
			data_on_disk += rm.m_bucket_data_on_disk; 
			data_in_memory += rm.m_bucket_data_in_memory; 
			total_load += rm.m_bucket_load; 
			max_load = std::max(max_load, rm.m_bucket_load);
			max_disk = std::max(max_disk, rm.m_bucket_data_on_disk);
			max_mem = std::max(max_mem, rm.m_bucket_data_in_memory);
		}
		pit->second.m_provider_metadata.m_data_on_disk = data_on_disk;
		pit->second.m_provider_metadata.m_data_in_memory = data_in_memory;
		pit->second.m_provider_metadata.m_total_load = total_load;
		pit->second.m_provider_metadata.m_max_load = max_load;
		pit->second.m_provider_metadata.m_max_data_disk = max_disk;
		pit->second.m_provider_metadata.m_max_data_mem = max_mem;
	}
	m_last_metadata_update = request_id;
	m_buckets_mtx->unlock();
}


void Worker::on_list_all_buckets(const tl::request& req, 
									size_t buff_size, 
									tl::bulk remote_bulk,
									int request_id) {
	
	std::pair<ReturnCode,size_t> status;
	
	update_metadata(request_id);

	tl::buffer buffer(0);
	tl::buffer_output_archive boa(buffer,get_engine());

	std::vector<BucketInfo> buckets;
	std::unordered_map<ProviderInfo,ProviderData,ProviderInfoHash>::iterator pit; 
	std::unordered_map<BucketTag,BucketData,BucketTagHash>::iterator rit;
	m_buckets_mtx->lock();
	for (pit = m_managed_buckets.begin(); pit != m_managed_buckets.end(); ++pit){
		for (rit = pit->second.m_buckets.begin(); rit != pit->second.m_buckets.end(); ++rit){
			buckets.push_back(rit->second.m_bucket_info);	
		}
	}
	m_buckets_mtx->unlock();
	
	boa & buckets;
	
	// Data would not fit in bulk
	if (buffer.size() > buff_size){

		status.first.m_error_code = BUFFER_SIZE;
		status.second = buffer.size();
		req.respond(status);
		return ;
	}

	tl::endpoint ep = req.get_endpoint();

	std::vector<std::pair<void*,std::size_t>> segments(1);
	segments[0].first  = (void*)(buffer.data());
	segments[0].second = buffer.size();
	tl::bulk local = get_engine().expose(segments, tl::bulk_mode::read_write);

	remote_bulk.on(ep) << local;
	
	status.second = buffer.size();
	
	req.respond(status);
}

struct CompareBucketInfo {
	double c_load = 0;
	double c_disk = 0;
	double c_mem = 0;

	/**
	 * @param wl Weight for the load-balancing
	 * @param ws Weight for the data-balancing
	 * @param n_disk Average amount of data per disk
	 * @param n_mem Average amount of data in memory
	 * @param n_load Average load
	 */
	CompareBucketInfo(double wl, double ws, 
			double n_disk, double n_mem, double n_load){
		// Switch WL and WS as the lower the weight is the more interesting
		// the parameter is
		if (n_load > 0)
			c_load = ws * ws / n_load / n_load;
		if (n_disk > 0)
			c_disk = wl * wl / n_disk / n_disk;
		if (n_mem > 0)
			c_mem  = wl * wl / n_mem  / n_mem;
	}
	
	
	bool operator() (const BucketInfo& r1, const BucketInfo& r2){
		double n1 = r1.m_bucket_metadata.m_bucket_data_on_disk
					* r1.m_bucket_metadata.m_bucket_data_on_disk
					* c_disk 
					+ r1.m_bucket_metadata.m_bucket_data_in_memory
					* r1.m_bucket_metadata.m_bucket_data_in_memory
					* c_mem		
					+ r1.m_bucket_metadata.m_bucket_load
					* r1.m_bucket_metadata.m_bucket_load
					* c_load;
		double n2 = r2.m_bucket_metadata.m_bucket_data_on_disk
					* r2.m_bucket_metadata.m_bucket_data_on_disk
					* c_disk
					+ r2.m_bucket_metadata.m_bucket_data_in_memory
					* r2.m_bucket_metadata.m_bucket_data_in_memory
					* c_mem
					+ r2.m_bucket_metadata.m_bucket_load
					* r2.m_bucket_metadata.m_bucket_load
					* c_load;
		return n1 > n2;
	}
};

void Worker::on_list_provider_buckets(const tl::request& req, 
										const std::string& service_name,
										double weight_load,
										double weight_data,
										size_t buff_size, 
										tl::bulk remote_bulk,
										int request_id) {

	std::pair<ReturnCode,size_t> status;
	
	update_metadata(request_id);

	tl::buffer buffer(0);
	tl::buffer_output_archive boa(buffer,get_engine());

	std::vector<ProviderSnapshot> buckets;

	std::unordered_map<ProviderInfo, ProviderData, ProviderInfoHash>::iterator it;
	m_buckets_mtx->lock();
	for (it = m_managed_buckets.begin(); it != m_managed_buckets.end(); ++it){
		if (it->first.m_service_name == service_name){
			buckets.push_back(ProviderSnapshot());

			ProviderSnapshot& ps = buckets[buckets.size()-1];
			
			ps.m_worker_info = m_this_workerInfo;
			ps.m_provider_metadata = it->second.m_provider_metadata;
			ps.m_provider_info = it->first;
			
			std::unordered_map<BucketTag,BucketData,
				BucketTagHash>::iterator rit;
			for (rit = it->second.m_buckets.begin(); 
					rit != it->second.m_buckets.end(); 
					++rit){
				ps.m_buckets.push_back(rit->second.m_bucket_info);	
			}

			double ave_load = ps.m_provider_metadata.m_total_load 
					/ ps.m_buckets.size();
			double ave_disk = ps.m_provider_metadata.m_data_on_disk 
					/ ps.m_buckets.size();
			double ave_mem  = ps.m_provider_metadata.m_data_in_memory 
					/ ps.m_buckets.size();

			// Sort bucketInfo by norm here to avoid sorting on the master
			std::sort(ps.m_buckets.begin(), 
					ps.m_buckets.end(), 
					CompareBucketInfo(weight_load,
						weight_data,
						ave_disk,
						ave_mem,
						ave_load));
		}
	}
	m_buckets_mtx->unlock();


	boa & buckets;
	
	// Data would not fit in bulk
	if (buffer.size() > buff_size){
		status.first.m_error_code = BUFFER_SIZE;
		status.second = buffer.size();
		req.respond(status);
		return ;
	}

	tl::endpoint ep = req.get_endpoint();

	std::vector<std::pair<void*,std::size_t>> segments(1);
	segments[0].first  = (void*)(buffer.data());
	segments[0].second = buffer.size();
	tl::bulk local = get_engine().expose(segments, tl::bulk_mode::read_write);

	remote_bulk.on(ep) << local;
	
	status.second = buffer.size();
	
	req.respond(status);
}

void Worker::on_list_providers(const tl::request& req, 
								size_t buff_size, 
								tl::bulk remote_bulk,
								int request_id) {
	
	std::pair<ReturnCode,size_t> status;

	update_metadata(request_id);

	std::unordered_map<ProviderInfo,ProviderMetadata,ProviderInfoHash> providers;
	std::unordered_map<ProviderInfo,ProviderData,ProviderInfoHash>::iterator pit; 
	m_buckets_mtx->lock();
	for (pit = m_managed_buckets.begin(); pit != m_managed_buckets.end(); ++pit){
		providers[pit->first] = pit->second.m_provider_metadata;	
	}
	m_buckets_mtx->unlock();
	
	tl::buffer buffer(0);
	tl::buffer_output_archive boa(buffer,get_engine());
	
	boa & providers;

	// Data would not fit in bulk
	if (buffer.size() > buff_size){
		status.first.m_error_code = BUFFER_SIZE;
		status.second = buffer.size();
		req.respond(status);
		return ;
	}

	tl::endpoint ep = req.get_endpoint();

	std::vector<std::pair<void*,std::size_t>> segments(1);
	segments[0].first  = (void*)(buffer.data());
	segments[0].second = buffer.size();
	tl::bulk local = get_engine().expose(segments, tl::bulk_mode::read_write);

	remote_bulk.on(ep) << local;
	
	status.second = buffer.size();
	
	req.respond(status);
}

ReturnCode Worker::on_initiate_rescaling(const std::string& service_name,
				int to_commission,
				const std::vector<ProviderInfo>& to_decommission){
	ReturnCode r;

	// TODO to add info if there are some commissions / decommissions
	// Pre rescaling function (last chance to do anything)
	if (m_provider_management[service_name].m_prerescaling){
		m_provider_management[service_name].m_prerescaling(
			m_provider_management[service_name].m_constructor_uargs);

	}


	// Get locks
	m_buckets_mtx->lock();

	// Check that the provider type exists
	if (m_provider_management.count(service_name) == 0){
		r.m_error_code = PROVIDER_TYPE_UNKNOWN;
		r.m_service_name = service_name;
		r.m_worker = m_this_workerInfo;
		r.interpret();
		m_buckets_mtx->unlock();
		return r;
	}
	
	// Check the providers to decommission exist and mark them
	for (const ProviderInfo& pi : to_decommission){
		// Check that the provider is managed
		if (m_managed_buckets.count(pi) == 0){
			m_buckets_mtx->unlock();
			r.m_error_code = error_codes::UNKNOWN_PROVIDER;
			r.m_worker = m_this_workerInfo;
			r.m_provider = pi;
			return r;
		} else {
			m_managed_buckets[pi].m_in_decommission = true;
		}
	}
	
	for (int i = 0; i < to_commission; ++i){
		ProviderInitializationData pid = m_provider_management[service_name]
											.m_initiate(m_provider_management[service_name].m_constructor_uargs);
		
		if (!pid.m_success){
			// Could not start the provider
			r.m_error_code = PROVIDER_NOT_COMMISSIONED;
			r.m_service_name = service_name;
			r.m_worker = m_this_workerInfo;
			r.interpret();
			m_buckets_mtx->unlock();
			return r;
		} else if (m_managed_buckets.count(pid.m_provider_info) > 0){
			// Returned provider is already managed
			r.m_error_code = PROVIDER_PRESENT;
			r.m_provider = pid.m_provider_info;
			r.m_worker = m_this_workerInfo;
			r.interpret();
			m_buckets_mtx->unlock();
			return r;
		} else {
			// Add to managed providers
			m_managed_buckets[pid.m_provider_info].m_provider_uargs = pid.m_provider_uargs;
		}
	}
	
	// Mark all as in_rescaling
	std::unordered_map<ProviderInfo, 
						ProviderData, 
						ProviderInfoHash>::iterator it; 
	for(it = m_managed_buckets.begin(); it != m_managed_buckets.end(); ++it){
		// Go through all providers 
		if (it->first.m_service_name == service_name){
			it->second.m_in_rescaling = true;
		}
	}
	m_need_metadata_update = true;
	m_buckets_mtx->unlock();

	return r;
}


ReturnCode Worker::on_terminate_rescaling(const std::string& service_name,
				const bool failed){

	ReturnCode r;

	m_buckets_mtx->lock();
	
	std::unordered_map<ProviderInfo, 
						ProviderData, 
						ProviderInfoHash>::iterator it; 
	for(it = m_managed_buckets.begin(); it != m_managed_buckets.end(); ){
		// Go through all providers 
		if (it->first.m_service_name == service_name){
			// Rescaling is over
			it->second.m_in_rescaling = false;
			if (it->second.m_in_decommission){
				it->second.m_in_decommission = false;
				if (!failed){
					// If it is in decommission, check if it can be destroyed
					if (it->second.m_buckets.size() > 0){
						// If no previous error, record this one
						if (!r){
							r.m_error_code = error_codes::BUCKETS_ON_PROVIDER;
							r.m_worker = m_this_workerInfo;
							r.m_provider = it->first;
						}
					} else {
						// Run the terminate function and erase it
						m_provider_management[service_name]
							.m_terminate(it->first,
								it->second.m_provider_uargs,
								m_provider_management[service_name].m_constructor_uargs);
						//std::cout << "Shutting down " << service_name << std::endl;
						it = m_managed_buckets.erase(it);
						continue;
					}
				} 
			} 
		}
		++it;
	}

	m_need_metadata_update = true;
	m_buckets_mtx->unlock();

	// post rescaling function
	if (m_provider_management[service_name].m_postrescaling){
		m_provider_management[service_name].m_postrescaling(
			m_provider_management[service_name].m_constructor_uargs);
	}
	
	return r;
}

void Worker::register_provider(const std::string& service_name,
							void* p_uargs,
							const InitiateProviderCallback& ipc, 
							const TerminateProviderCallback& tpc, 
							const PreRescalingCallback& pre,
							const PostRescalingCallback& post){
	assert(!m_has_joined);	
	if (m_provider_management.count(service_name) > 0){
		throw ProviderTypeAlreadyKnownException(m_this_workerInfo,service_name);
	}

	if (!ipc){
		throw CallbackNotCallableException();
	}
	if (!tpc){
		throw CallbackNotCallableException();
	}

	m_provider_management[service_name].m_service_name = service_name;
	m_provider_management[service_name].m_initiate = ipc;
	m_provider_management[service_name].m_terminate = tpc;
	m_provider_management[service_name].m_constructor_uargs = p_uargs;
	m_provider_management[service_name].m_prerescaling = pre;
	m_provider_management[service_name].m_postrescaling = post;

}


}
